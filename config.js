/*
Create a .env file (it must be gitignored)
    export PRIVATE_KEY_MAINNET=
Then, run the migration with:
    source .env && mcashbox migrate --network mainnet
*/
module.exports = {
  networks: {
    zeronet: {
      privateKey: 'be4936d871f031c065936bdc86e3ad379c50cbf73623d3e7c42f41682c82d558',
      userFeePercentage: 50,
      feeLimit: 1e9,
      fullHost: 'https://zeronet.mcash.network',
      network_id: '3'
    },
    compilers: {
      solc: {
        version: '0.4.25'
      }
    }
  }
};
