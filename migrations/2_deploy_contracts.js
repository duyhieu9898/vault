const VaultWithDailyLimit = artifacts.require('VaultWithDailyLimit.sol')
const VaultWithoutDailyLimit = artifacts.require('Vault.sol')
const VaultFactory = artifacts.require('VaultWithDailyLimitFactory.sol')

module.exports = function (deployer) {
  const args = process.argv.slice()
  if (process.env.DEPLOY_FACTORY) {
    deployer.deploy(VaultFactory)
    console.log('Factory with Daily Limit deployed')
  } else if (args.length < 5) {
    console.error('Multisig with daily limit requires to pass owner ' +
      'list, required confirmations and daily limit')
  } else if (args.length < 6) {
    deployer.deploy(VaultWithoutDailyLimit, args[3].split(','), args[4])
    console.log('Wallet deployed')
  } else {
    deployer.deploy(VaultWithDailyLimit, args[3].split(','), args[4], args[5])
    console.log('Wallet with Daily Limit deployed')
  }
}
