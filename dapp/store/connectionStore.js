import Connection from '../services/Connection'

export const state = () => ({
  isConnected: false
})

export const getters = {
  isConnected (state) {
    return state.isConnected
  }
}

export const actions = {
  updateConnectionStatus ({ commit }) {
    commit('UPDATE_CONNECTION_STATUS')
  }
}

export const mutations = {
  UPDATE_CONNECTION_STATUS (state) {
    state.isConnected = Connection.isConnected
  }
}
