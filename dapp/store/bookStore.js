import Vue from 'vue'

export const state = () => ({
  addressBook: {}
})

export const getters = {
  addressBook (state) {
    return state.addressBook
  }
}

export const actions = {
  syncAddressBookFromLocal ({ commit }) {
    const addressBook = JSON.parse(localStorage.getItem('addressBook') || '{}')
    commit('REPLACE_ADDRESS_BOOK', addressBook)
  },
  /**
   * Add an item to address book
   */
  addToBook ({ commit }, bookItem) {
    commit('ADD_TO_BOOK', bookItem)
  },
  /**
   * Remove an item from address book
   */
  onRemoveAddressFromBook ({ commit }, address) {
    commit('ON_REMOVE_ADDRESS_FROM_BOOK', address)
  },
  /**
   * Edit an address from address book
   */
  onEditAddressBook ({ commit }, item) {
    commit('ON_EDIT_ADDRESS_BOOK', item)
  }
}

export const mutations = {
  REPLACE_ADDRESS_BOOK (state, data) {
    state.addressBook = data
  },
  ADD_TO_BOOK (state, item) {
    Vue.set(state.addressBook, item.address, item)
    // Save new address into localStorage
    localStorage.setItem('addressBook', JSON.stringify(state.addressBook))
  },
  ON_REMOVE_ADDRESS_FROM_BOOK (state, address) {
    Vue.delete(state.addressBook, address)
    localStorage.setItem('addressBook', JSON.stringify(state.addressBook))
  },
  ON_EDIT_ADDRESS_BOOK (state, item) {
    Vue.set(state.addressBook, item.address, item)
    // Save new address into localStorage
    localStorage.setItem('addressBook', JSON.stringify(state.addressBook))
  }
}
