import Vue from 'vue'
import Wallet from '../services/Wallet'

export const state = () => ({
  wallets: {},
  totalItems: 0,
  updates: 0
})

export const getters = {
  wallets (state) {
    return state.wallets
  },
  totalItems (state) {
    return state.totalItems
  },
  updates (state) {
    return state.updates
  }
}

export const actions = {
  onUpdateWallets ({ commit }, wallets) {
    commit('ON_UPDATE_WALLETS', wallets)
  },
  onUpdateWallet ({ commit }, payload) {
    commit('ON_UPDATE_WALLET', payload)
  },
  onRemoveWallet ({ commit }, wallet) {
    commit('ON_REMOVE_WALLET', wallet)
  },
  onUpdateTotalWalletItems ({ commit }, totalItems) {
    commit('ON_UPDATE_TOTAL_ITEMS', totalItems)
  },
  onIncreaseUpdates ({ commit }) {
    commit('ON_INCREASE_UPDATES')
  },
  async updateParamsWallets ({ getters, dispatch }) {
    try {
      await dispatch('mcashStore/onEnabledMcashWebService', null, { root: true })

      const walletItems = getters.wallets ? Object.values(getters.wallets) : []
      if (walletItems.length) {
        Object.keys(getters.wallets).map(address => {
          Wallet.getBalance(address)
            .then(balance => {
              dispatch('onUpdateWallet', { address, value: { balance } })
            })
          Wallet.getRequired(address)
            .then(confirmations => {
              if (getters.wallets[address] && confirmations && confirmations > 0) {
                dispatch('onUpdateWallet', { address, value: { confirmations } })
              }
            })
            .catch(() => {
            // do nothing
            })

          /**
           * Get owners in order to verify whether or not the wallet
           * was created using the current network
           */
          Wallet.getOwners(address)
            .then(owners => {
              if (getters.wallets[address]) {
                dispatch('onUpdateWallet', { address, value: { isOnChain: owners.length > 0 } })
              }
            })
            .catch(() => {
              dispatch('onUpdateWallet', { address, value: { isOnChain: false } })
            })
          Wallet.getLimit(address)
            .then(limit => {
              if (limit && getters.wallets[address]) {
                dispatch('onUpdateWallet', { address, value: { limit } })
              }
            })
            .catch(() => {
              // do nothing
            })
          Wallet.calcMaxWithdraw(address)
            .then(maxWithdraw => {
              if (maxWithdraw && getters.wallets[address]) {
                dispatch('onUpdateWallet', { address, value: { maxWithdraw } })
              }
            })
            .catch(() => {
              // do nothing
            })
        })
      } else {
        dispatch('onUpdateTotalWalletItems', 0)
      }
    } catch (error) {
      console.log('%c updateParamsWallets', 'background: #009900; color: #fff', error)
    }
  }
}

export const mutations = {
  ON_UPDATE_WALLETS (state, data) {
    state.wallets = data
    console.log('data', state.wallets)
  },
  ON_UPDATE_WALLET (state, data) {
    const { address, value } = data
    const item = Object.assign({}, state.wallets[address] || {}, value || {})
    Vue.set(state.wallets, address, item)
  },
  ON_REMOVE_WALLET (state, wallet) {
    Vue.delete(state.wallets, wallet)
  },
  ON_UPDATE_TOTAL_ITEMS (state, totalItems) {
    state.totalItems = totalItems
  },
  ON_INCREASE_UPDATES (state) {
    state.updates += 1
  }
}
