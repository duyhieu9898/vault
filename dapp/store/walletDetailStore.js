import Vue from 'vue'
import Wallet from '../services/Wallet'
import McashWebService from '../services/McashWebService'

export const state = () => ({
  wallet: {},
  userTokens: {},
  owners: [],
  walletInfo: {
    balance: 0,
    confirmations: 0
  },
  transactions: {},
  transactionPagination: {
    totalItems: 0,
    itemsPerPage: 0,
    currentPage: 0
  },
  showPending: true,
  showExecuted: true
})

export const getters = {
  wallet (state) {
    return state.wallet
  },
  walletInfo (state) {
    return state.walletInfo
  },
  totalTokens (state) {
    return state.wallet && state.wallet.tokens ? Object.keys(state.wallet.tokens).length : 0
  },
  userTokens (state) {
    return state.userTokens
  },
  owners (state) {
    return state.owners
  },
  showPending (state) {
    return state.showPending
  },
  showExecuted (state) {
    return state.showExecuted
  },
  transactions (state) {
    return state.transactions
  },
  transactionPagination (state) {
    return state.transactionPagination
  }
}

export const actions = {
  onSetWallet ({ commit }, wallet) {
    commit('SET_WALLET', wallet)
  },
  onSetUserTokens ({ commit }, userTokens) {
    commit('SET_USER_TOKENS', userTokens)
  },
  onUpdateUserTokenById ({ commit }, { id, value }) {
    commit('UPDATE_USER_TOKEN_BY_ID', { id, value })
  },
  toggleShowPending ({ commit }, value) {
    commit('TOGGLE_SHOW_PENDING', value)
  },
  toggleShowExecuted ({ commit }, value) {
    commit('TOGGLE_SHOW_EXECUTED', value)
  },
  updateParamsWalletDetail ({ commit, dispatch, getters }) {
    const address = getters.wallet.address
    // get owners
    Wallet.getOwners(address)
      .then(owners => {
        commit('SET_OWNERS', McashWebService.toChecksumAddress(owners))
        // Check if the owners are in the wallet.owners object
        const walletOwnersKeys = getters.wallet.owners ? Object.keys(getters.wallet.owners) : []

        if (!getters.wallet.owners) {
          commit('SET_WALLET_OWNERS', {})
          getters.owners.forEach(function (item, index) {
            commit('UPDATE_WALLET_OWNERS_BY_ADDRESS', {
              id: item,
              value: { 'name': `Owner ${index + 1}`, 'address': item }
            })
            if (index === getters.owners.length - 1) { // last item
              Wallet.updateWallet(getters.wallet)
            }
          })
        } else {
          getters.owners.forEach((item, index) => {
            // If owner not in list
            if (walletOwnersKeys.indexOf(item) === -1) {
              commit('UPDATE_WALLET_OWNERS_BY_ADDRESS', {
                id: item,
                value: { 'name': `Owner ${index + 1}`, 'address': item }
              })
            } else if (!getters.wallet.owners[item].name) {
              // Set owner name with its address
              // $scope.wallet.owners[$scope.owners[x]].name = $scope.wallet.owners[$scope.owners[x]].address
              commit('UPDATE_WALLET_OWNERS_BY_ADDRESS', {
                id: item,
                value: Object.assign({}, getters.wallet.owners[item], { name: `Owner ${index + 1}` })
              })
            }
          })
        }
      })
      .catch(() => {
        // do nothing
      })
    // get balance
    Wallet.getBalance(address)
      .then(balance => {
        commit('UPDATE_WALLET_INFO', { key: 'balance', value: balance })
      })
      .catch(() => {
        // do nothing
      })
    // get required confirmations
    Wallet.getRequired(address)
      .then(confirmations => {
        commit('UPDATE_WALLET_INFO', { key: 'confirmations', value: confirmations })
      })
      .catch(() => {
        // do nothing
      })
    // Get Transaction Count
    Wallet.getTransactionCount(address)
      .then(total => {
        commit('UPDATE_TRANSACTION_PAGINATION', { totalItems: total })
        dispatch('updateTransactions')
      })
      .catch(e => {
        // do nothing
        console.error(e)
      })
  },
  updateTransactions ({ state, getters, commit }) {
    const { totalItems, itemsPerPage, currentPage } = getters.transactionPagination
    // Get all transaction ids, with filters
    const address = getters.wallet.address
    const from = totalItems - itemsPerPage * (currentPage)
    const to = totalItems - (currentPage - 1) * itemsPerPage

    Wallet.getTransactionIds(address, from > 0 ? from : 0, to, getters.showPending, getters.showExecuted)
      .then(ids => {
        console.log('%c getTransactionIds...', 'background: #009900; color: #fff', ids)
        if (Array.isArray(ids)) {
          ids.map((txId) => {
            const transactionItem = getters.transactions[txId] || {}
            // get transaction info
            Wallet.getTransaction(address, txId)
              .then(info => {
                console.log('%c getTransaction...', 'background: #009900; color: #fff', info)
              })
            // Get transaction confirmations
            Wallet.getConfirmations(address, txId)
              .then(confirmations => {
                transactionItem.confirmations = McashWebService.toChecksumAddress(confirmations)
                // If the current user is among the array of confirmations, we can set the transaction
                // as confirmed by that user
                transactionItem.confirmed = transactionItem.confirmations.indexOf(McashWebService.coinbase) !== -1
              })
            // ======
            commit('UPDATE_TRANSACTION_ITEM', { key: txId, value: transactionItem })
          })
        }
      })
  }
}

export const mutations = {
  SET_WALLET (state, data) {
    state.wallet = data
  },
  SET_WALLET_OWNERS (state, value = {}) {
    Vue.set(state.wallet, 'owners', value)
  },
  UPDATE_WALLET_OWNERS_BY_ADDRESS (state, { key, value }) {
    if (!state.wallet.owners) {
      Vue.set(state.wallet, 'owners', {})
    }
    Vue.set(state.wallet.owners, key, value)
  },
  SET_USER_TOKENS (state, data) {
    state.userTokens = data
  },
  UPDATE_USER_TOKEN_BY_ID (state, { id, value }) {
    const item = Object.assign({}, state.userTokens[id] || {}, value || {})
    Vue.set(state.userTokens, id, item)
  },
  SET_OWNERS (state, owners) {
    state.owners = owners
  },
  UPDATE_WALLET_INFO (state, { key, value }) {
    Vue.set(state.walletInfo, key, value)
  },
  TOGGLE_SHOW_PENDING (state, value) {
    state.showPending = value
  },
  TOGGLE_SHOW_EXECUTED (state, value) {
    state.showExecuted = value
  },
  UPDATE_TRANSACTION_PAGINATION (state, pagination) {
    state.transactionPagination = Object.assign({}, state.transactionPagination || {}, pagination)
  },
  UPDATE_TRANSACTION_ITEM (state, { key, value }) {
    Vue.set(state.transactions, key, value)
  }
}
