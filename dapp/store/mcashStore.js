import mcashUtils from '../lib/multicoin-utils/mcashUtils'
import { abbreviateNumber } from '../utils/helper'
import { fromAmount } from '../lib/multicoin-utils/common'
import McashWebService from '../services/McashWebService'
import Transaction from '../services/Transaction'

export const state = () => ({
  enabled: null,
  address: '',
  network: {
    type: null
  },
  balance: null,
  enabledDialogError: null,
  enabledMcashWebService: null,
  mcashscan: '',
  mcashLightInitiated: false
})

export const getters = {
  enabledMcash (state) {
    return state.enabled
  },
  addressMcash (state) {
    return state.address
  },
  networkMcash (state) {
    return state.network || {}
  },
  balanceMcash (state) {
    return abbreviateNumber(state.balance)
  },
  enabledDialogError (state) {
    return state.enabledDialogError
  },
  explorerTx: (state) => (id) => {
    return state.mcashscan + '/transaction/' + id
  },
  enabledMcashWebService (state) {
    return state.enabledMcashWebService
  },
  mcashLightInitiated (state) {
    return state.mcashLightInitiated
  }
}

export const actions = {
  getSyncAddressMcash ({ dispatch }) {
    return dispatch('connectToMcashProvider')
      .then(res => res)
      .catch(() => false)
  },
  async getProviderMcash ({ commit, getters }) {
    if (getters.enabledMcash) {
      return true
    }
    await mcashUtils.onMcashInitialized()
    commit('ENABLE', true)
    return true
  },
  getAddressMcash ({ commit }) {
    const address = mcashUtils.getAddress()
    commit('GET_WALLET_ADDRESS', address)
    return address
  },
  getNetworkMcash ({ commit }) {
    const network = mcashUtils.getNetwork()
    commit('NETWORK', network)
    return network
  },
  getBalanceMcash ({ commit }) {
    return mcashUtils.getBalance()
      .then((res) => {
        const balance = fromAmount(res)
        commit('GET_BALANCE', balance)
        return balance
      })
      .catch(() => {})
  },
  async connectToMcashProvider ({ commit, dispatch }) {
    try {
      if (navigator.userAgent.search('Midas') !== -1) { // is mobile
        commit('ENABLE_MCASH', true)
      } else {
        await dispatch('getProviderMcash')
        await mcashUtils.enable()
      }
      dispatch('getNetworkMcash')
      const address = await dispatch('getAddressMcash')
      dispatch('getBalanceMcash')
      return address
    } catch (error) {
      return Promise.reject(error)
    }
  },
  handleMcashMessage ({ commit, dispatch }, message) {
    const { action } = message || {}
    if (action === 'tabReply') {
      commit('SET_MCASHLIGHT_INITIALIZED')
    }
    if (action === 'setAccount') {
      dispatch('getAddressMcash')
      const network = mcashUtils.getNetwork()
      if (network) {
        dispatch('getNetworkMcash')
        dispatch('getBalanceMcash')
      }
    } else if (action === 'setNode') {
      const network = mcashUtils.getNetwork()
      if (network) {
        dispatch('getNetworkMcash')
        dispatch('getBalanceMcash')
      }
      dispatch('onGetMcashchain')
    }
  },
  sendTransactionMcash ({ dispatch }, payload) {
    const { amount, to, memo, tokenId } = payload
    return mcashUtils.sendToken(to, amount, tokenId, memo)
      .then(res => res)
      .catch(error => {
        throw error
      })
  },
  openDialogRequiredMcash ({ commit }) {
    commit('GET_ENABLED_DIALOG_ERROR', true)
  },
  closeDialogError ({ commit }) {
    commit('GET_ENABLED_DIALOG_ERROR', false)
  },
  onEnabledMcashWebService ({ commit, getters, dispatch }) {
    return new Promise(async (resolve, reject) => {
      try {
        await dispatch('getProviderMcash')
        if (getters.enabledMcashWebService) {
          resolve(true)
        } else {
          await McashWebService.webInitialized()
          commit('SET_ENABLED_MCASH_WEB')
          resolve(true)
        }
      } catch (error) {
        reject(error)
      }
    })
  },
  async onGetMcashchain ({ commit }) {
    const data = await Transaction.getMcashchain() || {}
    commit('ON_GET_MCASHSCAN', data)
  }
}

export const mutations = {
  GET_WALLET_ADDRESS (state, address) {
    state.address = address
  },
  ENABLE (state, enabled) {
    state.enabled = enabled
  },
  NETWORK (state, network) {
    state.network = network
  },
  GET_BALANCE (state, balance) {
    state.balance = balance
  },
  GET_ENABLED_DIALOG_ERROR (state, value) {
    state.enabledDialogError = value
  },
  SET_ENABLED_MCASH_WEB (state) {
    state.enabledMcashWebService = true
  },
  ON_GET_MCASHSCAN (state, data) {
    const { mcashscan } = data
    state.mcashscan = mcashscan
  },
  SET_MCASHLIGHT_INITIALIZED (state) {
    state.mcashLightInitiated = true
  }
}
