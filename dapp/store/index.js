const state = () => ({
  dialogRestoreWallet: null
})

const getters = {
  dialogRestoreWallet (state) {
    return state.dialogRestoreWallet
  }
}

const actions = {
  setDialogRestoreWallet ({ commit }, value) {
    commit('SET_DIALOG_RESTORE_WALLET', value)
  }
}

const mutations = {
  SET_DIALOG_RESTORE_WALLET (state, value) {
    state.dialogRestoreWallet = value
  }
}

export {
  state,
  getters,
  actions,
  mutations
}
