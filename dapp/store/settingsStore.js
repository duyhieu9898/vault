import Config from '../services/Config'
import Transaction from '../services/Transaction'
// import CommunicationBus from '../services/CommunicationBus'
import { loadConfiguration, txDefault, txDefaultOrig } from '../config'

const state = () => ({
  configBlacklist: [
    'mcashNodes',
    'selectedMcashNode',
    'walletFactoryAddresses',
    'selectedWalletFactoryAddress'
  ],
  config: {
    mcashNode: {
      name: null,
      url: null
    },
    wallet: null,
    mcashNodes: [],
    walletFactoryAddress: {
      name: null,
      address: null
    },
    walletFactoryAddressList: [],
    walletFactoryAddresses: {},
    selectedMcashNode: {
      name: null,
      url: null
    }
  }
})

const getters = {
  config (state) {
    return state.config
  },
  configBlacklist (state) {
    return state.configBlacklist
  }
}

const actions = {
  /**
    * Loads configuration
    */
  async loadConfig ({ commit, dispatch }) {
    try {
      const config = Config.getUserConfiguration()
      // Maps constables used by ui-select
      const selectedMcashNode = config.mcashNodes.filter(function (item) { return item.url === config.mcashNode })
      config.selectedMcashNode = {
        url: config.mcashNode,
        name: selectedMcashNode.length === 1 ? selectedMcashNode[0].name : 'Custom node'
      }

      for (const x in config.mcashNodes) {
        const item = config.mcashNodes[x]

        if (item.url === config.mcashNode) {
          selectedMcashNode.name = item.name
          break
        }
      }
      const factoryAddress = {}
      factoryAddress.name = 'Automatic'
      // Automatically get MCASH chain
      await dispatch('mcashStore/onEnabledMcashWebService', null, { root: true })
      const data = await Transaction.getMcashchain()

      if (data.chain === 'testnet') {
        factoryAddress.address = config.walletFactoryAddresses.testnet.address
      } else if (data.chain === 'zeronet') {
        factoryAddress.address = config.walletFactoryAddresses.zeronet.address
      } else {
        console.log(config.walletFactoryAddresses.mainnet)
        factoryAddress.address = config.walletFactoryAddresses.mainnet.address
      }

      // Get the current wallet factory address object,
      // we have to search inside the walletFactoryAddresses object
      const walletFactoryKeys = Object.keys(config.walletFactoryAddresses)
      let currentWalletFactory

      for (let x = 0; x < walletFactoryKeys.length; x++) {
        if (config.walletFactoryAddresses[walletFactoryKeys[x]].address === config.walletFactoryAddress) {
          currentWalletFactory = config.walletFactoryAddresses[walletFactoryKeys[x]]
          break
        }
      }

      // Set the new wallet factory address only if the Automatic mode is setted, currentWalletFactory
      // is undefined when using custom addresses
      if (currentWalletFactory) {
        config.walletFactoryAddress = factoryAddress
      } else {
        config.walletFactoryAddress = {
          'address': config.walletFactoryAddress,
          'name': 'Custom contract'
        }
      }
      config.walletFactoryAddressList = [factoryAddress] // Needed by the ui drop-down select list
      commit('SET_CONFIG', config)
    } catch (error) {
      console.log('error', error)
      // do nothing
    }
  },
  setAppVersion () {
    // if (isElectron) {
    //   $scope.appVersion = require('electron').remote.app.getVersion();
    // } else {
    //   $http.get('package.json').success(function (package_json){
    //     $scope.appVersion = package_json.version;
    //   });
    // }
  },
  /**
  * Updates configuration
  */
  update ({ getters, dispatch }) {
    try {
      // Current saved configuration
      const previousConfig = Config.getConfiguration('userConfig')

      // Create a config copy
      const configCopy = Object.assign({}, getters.config, previousConfig)

      // Wraps selectedMcashNode.url to mcashNode
      // See reverse mapping in loadConfig()

      if (configCopy.selectedMcashNode) {
        if (!configCopy.selectedMcashNode.url) {
          dispatch('notificationStore/showErrorNotification', 'Please specify an ethereum node.', { root: true })
          return
        }
        configCopy.mcashNode = configCopy.selectedMcashNode.url
      }

      // Wraps selectedWalletFactoryAddress.address to walletFactoryAddress
      if (configCopy.walletFactoryAddress) {
        if (!getters.config.walletFactoryAddress.address) {
          dispatch('notificationStore/showErrorNotification', 'Please specify a wallet factory contract.', { root: true })
          return
        }
        configCopy.walletFactoryAddress = getters.config.walletFactoryAddress.address
      }

      // Check blacklist values
      Object.keys(configCopy).map(function (item) {
        if (getters.configBlacklist.indexOf(item) !== -1) {
          delete configCopy[item]
        }
      })
      // Save new configuation
      Config.setConfiguration('userConfig', JSON.stringify(configCopy))

      loadConfiguration() // config.js

      // Reload web3 provider only if it has been updated in the configuration,
      // Otherwise update specific providers individually,
      // this allows us to avoid providing connection/show setup to Ledger/Trezor
      // Also use CommunicationBus to stop undergoing $interval functions (like updateInfo, accounts watchers)
      // CommunicationBus.stopInterval('updateInfo')
      // TODO: connect provider remote
      // const startUpdateInfoInterval = true // start interval by default by the end of the flow

      // if (McashWebService.engine) {
      //   McashWebService.engine.stop()
      // }

      // if (!previousConfig || configCopy.wallet !== previousConfig.wallet) {
      //   startUpdateInfoInterval = false
      //   McashWebService.webInitialized().then(function () {
      //     CommunicationBus.startInterval('updateInfo', configCopy.accountsChecker.checkInterval)
      //     // Execute function immediately in order to update the UI as soon as possible
      //     CommunicationBus.getFn('updateInfo')()
      //   })
      // } else if (configCopy.wallet === 'lightwallet') {
      //   McashWebService.lightWalletSetup()
      // } else {
      //   McashWebService.web3 = new MultisigWeb3(new MultisigWeb3.providers.HttpProvider(configCopy.mcashNode))
      // }

      // if (McashWebService.engine) {
      //   McashWebService.engine.start()
      // }

      // if (startUpdateInfoInterval) {
      //   CommunicationBus.startInterval('updateInfo', configCopy.accountsChecker.checkInterval)
      //   // Execute function immediately in order to update the UI as soon as possible
      //   CommunicationBus.getFn('updateInfo')()
      // }

      // // If we're using lightwallet for 1st time,
      // // redirect the user to accounts/add page
      // if (configCopy.wallet === 'lightwallet' && previousConfig.wallet !== 'lightwallet' && !Config.getConfiguration('accounts')) {
      //   Config.setConfiguration('showCreateWalletModal', true)
      //   $location.path('/accounts')
      // }

      // Show 'success' notification
      dispatch('notificationStore/showSuccessNotification', 'Configuration updated successfully.', { root: true })
    } catch (error) {
      dispatch('notificationStore/showErrorNotification', error, { root: true })
    }
  },

  /**
  * Adds a new custom ui-select item to Ethereum Node
  */
  addCustomMcashNode ({ commit }, input) {
    const selectedMcashNode = {
      url: input,
      name: input,
      isCustom: true
    }
    commit('SET_CONFIG', { selectedMcashNode })
  },

  /**
  * Adds a new custom ui-select item to Wallet Factory Address
  */
  addCustomWalletFactoryAddress ({ commit }, input) {
    const walletFactoryAddress = {
      address: input
    }
    commit('SET_CONFIG', { walletFactoryAddress })
  },

  /**
  * Open reset settings modal
  */
  reset  ({ dispatch }) {
    Config.removeConfiguration('userConfig')
    Object.assign(txDefault, txDefaultOrig)
    dispatch('loadConfig')
      .then(() => {
        dispatch('notificationStore/showSuccessNotification', 'Configuration reseted successfully.', { root: true })
      })
      .catch((e) => {
        dispatch('notificationStore/showErrorNotification', e, { root: true })
      })
  },

  // vue
  onChangeNode ({ commit, getters }, urlNode) {
    const selectedMcashNode = getters.config.mcashNodes.find((e) => e.url === urlNode)
    commit('SET_CONFIG', { selectedMcashNode })
  },
  onChangeFactoryContractAddress ({ commit, getters }, address) {
    const walletFactoryAddress = getters.config.walletFactoryAddressList.find((e) => e.address === address)
    commit('SET_CONFIG', { walletFactoryAddress })
  }
}

const mutations = {
  SET_CONFIG (state, value) {
    state.config = Object.assign({}, state.config, value)
  }
}

export {
  state,
  getters,
  actions,
  mutations
}
