export const state = () => ({
  isShowing: false,
  color: '',
  timeout: 5000,
  text: '',
  top: true,
  right: true,
  bottom: false,
  left: false
})

export const getters = {
  isShowing (state) { return state.isShowing },
  color (state) { return state.color },
  timeout (state) { return state.timeout },
  text (state) { return state.text },
  top (state) { return state.top },
  right (state) { return state.right },
  bottom (state) { return state.bottom },
  left (state) { return state.left }
}

export const actions = {
  showNotification ({ commit }, payload) {
    commit('SHOW_NOTIFICATION', payload)
  },
  closeNotification ({ commit }) {
    commit('CLOSE_NOTIFICATION')
  },
  showErrorNotification ({ commit }, payload = {}) {
    commit('SHOW_ERROR_NOTIFICATION', payload)
  },
  showSuccessNotification ({ commit }, payload = {}) {
    commit('SHOW_SUCCESS_NOTIFICATION', payload)
  }
}

export const mutations = {
  SHOW_NOTIFICATION (state, payload) {
    state.isShowing = true
    state.text = payload.text
    if (payload.color) {
      state.color = payload.color
    }
  },
  CLOSE_NOTIFICATION (state) {
    state.isShowing = false
    state.text = ''
  },
  SHOW_ERROR_NOTIFICATION (state, payload) {
    state.isShowing = true
    if (typeof payload === 'string') {
      state.text = payload
      state.color = 'error'
    } else {
      state.text = payload.text || 'Something went wrong!'
      state.color = payload.color || 'error'
    }
  },
  SHOW_SUCCESS_NOTIFICATION (state, payload) {
    state.isShowing = true
    if (typeof payload === 'string') {
      state.text = payload
      state.color = 'success'
    } else {
      state.text = payload.text || 'Something went wrong!'
      state.color = payload.color || 'success'
    }
  }
}
