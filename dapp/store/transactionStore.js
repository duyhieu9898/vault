export const state = () => ({
  updates: 0
})

export const getters = {
  updates (state) {
    return state.updates
  }
}

export const actions = {
  onIncreaseUpdates ({ commit }) {
    commit('ON_INCREASE_UPDATES')
  }
}

export const mutations = {
  ON_INCREASE_UPDATES (state) {
    state.updates += 1
  }
}
