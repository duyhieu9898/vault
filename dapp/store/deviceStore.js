export const state = () => ({
  isMobile: false,
  isMidasWallet: false
})

export const getters = {
  deviceIsMobile (state) {
    return state.isMobile
  },
  deviceIsMidasWallet (state) {
    return state.isMidasWallet
  }
}

export const actions = {
  onToggleIsMobile ({ commit }, isMobile) {
    commit('ON_TOGGLE_IS_MOBILE', isMobile)
  },
  onToggleIsMidasWallet ({ commit }, isMidasWallet) {
    commit('ON_TOGGLE_IS_MIDAS_WALLET', isMidasWallet)
  }
}

export const mutations = {
  ON_TOGGLE_IS_MOBILE (state, isMobile) {
    state.isMobile = isMobile
  },
  ON_TOGGLE_IS_MIDAS_WALLET (state, isMidasWallet) {
    state.isMidasWallet = isMidasWallet
  }
}
