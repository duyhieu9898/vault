export const state = () => ({
  coinbase: null
})

export const getters = {
  coinbase (state) {
    return state.coinbase
  }
}

export const actions = {
  setCoinBase ({ commit }, coinbase) {
    commit('SET_COIN_BASE', coinbase)
  }
}

export const mutations = {
  SET_COIN_BASE (state, coinbase) {
    state.coinbase = coinbase
  }
}
