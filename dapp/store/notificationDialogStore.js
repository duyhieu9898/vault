export const state = () => ({
  message: null,
  desc: null
})

export const getters = {
  messageDialogNotification (state) {
    return state.message
  },
  descDialogNotification (state) {
    return state.desc
  }
}

export const actions = {
  resetDialogMessage ({ commit }) {
    commit('RESET_DIALOG_NOTIFICATION')
  },
  showDialogErrorNotification ({ commit }, message) {
    commit('SHOW_DIALOG_ERROR_NOTIFICATION', message)
  },
  showDialogSuccessNotification ({ commit }, message) {
    commit('SHOW_DIALOG_SUCCESS_NOTIFICATION', message)
  }
}

export const mutations = {
  SHOW_DIALOG_ERROR_NOTIFICATION (state, message) {
    state.message = typeof message === 'string' ? message : message.text
    state.desc = typeof message === 'string' ? '' : message.desc
    state.status = 'error'
  },
  SHOW_DIALOG_SUCCESS_NOTIFICATION (state, message) {
    state.message = typeof message === 'string' ? message : message.text
    state.desc = typeof message === 'string' ? '' : message.desc
    state.status = 'success'
  },
  RESET_DIALOG_NOTIFICATION (state) {
    state.message = null
    state.desc = null
    state.status = null
  }
}
