const compareVersions = function (v1, v2, operator) {
  const allowedOperators = ['>', '>=', '=', '<', '<=']
  const comparator = ['==', '==='].indexOf(operator) > -1 ? '=' : operator
  if (allowedOperators.indexOf(comparator) === -1) {
    throw new Error('Invalid operator, expected one of ' + allowedOperators.join('|'))
  }
  const v1parts = v1 ? v1.split('.') : []
  const v2parts = v2 ? v2.split('.') : []
  const maxLen = Math.max(v1parts.length, v2parts.length)
  let part1, part2
  let cmp = 0
  for (let i = 0; i < maxLen && !cmp; i++) {
    part1 = parseInt(v1parts[i], 10) || 0
    part2 = parseInt(v2parts[i], 10) || 0
    if (part1 > part2) {
      cmp = 1
    }
    if (part1 < part2) {
      cmp = -1
    }
  }
  const operatorResMap = { '>': [1], '>=': [0, 1], '=': [0], '<=': [-1, 0], '<': [-1] }
  return operatorResMap[comparator].indexOf(cmp) > -1
}

class MidasWallet {
  constructor () {
    this.userAgent = null
    this.isMidasWallet = false
    this.version = null
  }

  init = () => {
    const userAgent = navigator.userAgent
    this.userAgent = userAgent
    this.isMidasWallet = !!userAgent.match(/Midas/i)
    if (this.isMidasWallet) {
      const result = /Midas\/([0-9.]*)/.exec(navigator.userAgent)
      if (result) {
        this.version = result[1]
      }
    }
  }

  getDevice = () => {
    if (this.userAgent.match(/Android/i)) {
      return 'Android'
    }
    if (this.userAgent.match(/iPhone|iPad|iPod/i)) {
      return 'iOS'
    }
    return ''
  }

  isSupportedMcashchainMemo = () => {
    if (!this.isMidasWallet) {
      return true
    }
    const device = this.getDevice()
    let supported = false
    try {
      switch (device) {
        case 'Android':
          supported = compareVersions(this.version, '1.6.8.4', '>=')
          break
        case 'iOS':
          supported = compareVersions(this.version, '1.6.7', '>=')
          break
      }
    } catch (e) {
      console.log(e)
    }
    return supported
  }
}

export default new MidasWallet()
