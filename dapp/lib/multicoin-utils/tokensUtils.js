import jsonTokens from './tokens.json'
import { toAmount, fromAmount } from './common'

class TokensUtils {
  constructor () {
    this.tokens = jsonTokens
  }

  findTokenBySymbol (tokenSymbol) {
    return tokenSymbol ? this.tokens[tokenSymbol.toUpperCase()] : null
  }

  toTokenAmountBySymbol (value, tokenSymbol) {
    const finder = this.findTokenBySymbol(tokenSymbol)
    return finder ? toAmount(value, finder.decimals) : 0
  }

  fromTokenAmountBySymbol (value, tokenSymbol) {
    const finder = this.findTokenBySymbol(tokenSymbol)
    return finder ? fromAmount(value, finder.decimals) : 0
  }
}

export default new TokensUtils()
