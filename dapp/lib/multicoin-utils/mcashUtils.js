import { isMidasWallet } from './common'

const networkEnum = {
  mainnet: { id: 1, type: 'mainnet' },
  testnet: { id: 2, type: 'testnet' },
  zeronet: { id: 3, type: 'zeronet' }
}

class McashUtils {
  constructor () {
    this.networkEnum = networkEnum
    this.mainnetId = 1
    this.currentNetwork = null
    this.defaultAddress = null
  }

  init (config) {
    if (typeof config.network === 'number') {
      this.mainnetId = config.network
    }
  }

  hasProvider () {
    return window && typeof window.mcashWeb !== 'undefined'
  }

  checkProvider () {
    if (!this.hasProvider()) {
      throw new Error('mcashWeb is not supported')
    }
  }

  onMcashInitialized () {
    return new Promise((resolve, reject) => {
      if (this.hasProvider()) {
        resolve(this.hasProvider())
      } else {
        let count = 0
        const retry = () => {
          const timeout = setTimeout(() => {
            count++
            if (count === 10) {
              clearTimeout(timeout)
              reject('Non-Mcashchain browser detected. Install McashLight and try again')
            }
            if (this.hasProvider()) {
              resolve(true)
            } else {
              retry()
            }
          }, 500)
        }
        retry()
      }
    })
  }

  async enable () {
    return new Promise((resolve, reject) => {
      if (this.getAddress()) {
        resolve(this.getAddress())
      } else {
        let count = 0
        const retry = () => {
          const timeout = setTimeout(() => {
            count++
            if (count === 10) {
              clearTimeout(timeout)
              reject('Non-Mcashchain browser detected. You should consider trying McashLight!')
              return
            }
            if (this.getAddress()) {
              resolve(this.getAddress())
            } else {
              retry()
            }
          }, 500)
        }
        retry()
      }
    })
  }

  prettyErrorMessage (message) {
    const errorMessageRegex = new RegExp('class [a-zA-Z._$]+ :')
    return message.replace(errorMessageRegex, '').trim()
  }

  isValidAddress (address) {
    this.checkProvider()
    try {
      return window.mcashWeb.utils.crypto.isAddressValid(address)
    } catch (e) {
      console.error(e)
      return false
    }
  }

  getAddress () {
    const addr = this.hasProvider() ? window.mcashWeb.defaultAddress.base58 : null
    this.defaultAddress = addr
    return addr
  }

  getBalance (address) {
    this.checkProvider()
    return window.mcashWeb.mcash.getBalance(address)
  }

  toHex (str) {
    this.checkProvider()
    return window.mcashWeb.toHex(str)
  }

  getNetwork () {
    this.checkProvider()
    let fullNode = window.mcashWeb.fullNode
    if (/.\/$/.test(fullNode)) {
      fullNode = fullNode.substr(0, fullNode.length - 1)
    }
    let network = null
    switch (fullNode.host) {
      case 'https://mainnet.mcash.network':
        network = this.networkEnum.mainnet
        break
      case 'https://testnet.mcash.network':
        network = this.networkEnum.testnet
        break
      case 'https://zeronet.mcash.network':
        network = this.networkEnum.zeronet
        break
    }
    this.currentNetwork = network
    return network
  }

  validateProvider (options = {}) {
    const { walletDeepLink } = options
    const mainnet = this.networkEnum.find(a => a.id === this.mainnetId)
    const errorEnum = {
      loginTo: {
        pc: {
          text: 'Log in to McashLight'
        },
        mobile: {
          text: 'Log in to Midas Wallet'
        }
      },
      switchTo: {
        pc: {
          text: `Switch to Mcashchain (${mainnet ? mainnet.type : 'mainnet'})`
        },
        mobile: {
          text: `Switch to Mcashchain (${mainnet ? mainnet.type : 'mainnet'})`
        }
      }
    }
    if (isMidasWallet()) {
      if (!this.hasProvider()) {
        return errorEnum.switchTo
      }
      return true
    }
    if (!this.hasProvider()) {
      return {
        pc: {
          text: 'Install McashLight',
          desc: 'You need a Mcashchain-compatible wallet to play our games',
          actionLink: {
            name: 'Get McashLight',
            href: 'http://bit.ly/mcashchain-chrome-extension'
          }
        },
        mobile: {
          text: 'Use Midas Wallet',
          desc: 'You need a Mcashchain app to play our games - we recommend using Midas Wallet',
          actionLink: {
            name: 'Open in Midas Wallet',
            href: walletDeepLink || 'https://midaswallet.page.link/Gt3d'
          }
        }
      }
    }
    if (!this.getAddress()) {
      return errorEnum.loginTo
    }
    const currNetId = this.currentNetwork && this.currentNetwork.id
    if (currNetId !== this.mainnetId) {
      if (!this.hasProvider()) {
        return errorEnum.switchTo
      }
    }
    return true
  }

  messageCallback (event, callback) {
    if (event && event.data && event.data.message) {
      const { message, isMcashLight } = event.data
      if (!isMcashLight) {
        return
      }
      const { action, data } = message || {}
      if (action === 'setAccount' && this.isValidAddress(data) && data !== this.defaultAddress) {
        this.getAddress()
      } else if (action === 'setNode') {
        this.getNetwork()
      }
      callback(message)
    }
  }

  addListenerForUpdate (callback) {
    window.addEventListener('message', (e) => {
      this.messageCallback(e, callback)
    })
  }

  removeListenerForUpdate (callback) {
    window.removeEventListener('message', (e) => {
      this.messageCallback(e, callback)
    })
  }

  signMessage (message) {
    return new Promise(async (resolve, reject) => {
      try {
        this.checkProvider()
        const messageHex = this.toHex(message)
        const signature = await window.mcashWeb.mcash.sign(messageHex)
        return resolve({
          signature,
          address: this.getAddress()
        })
      } catch (e) {
        reject(e)
      }
    })
  }

  async sendToken (toAddress, amount, tokenId = 0, memo = '') {
    try {
      this.checkProvider()
    } catch (e) {
      return Promise.reject(e.message)
    }
    let result = null
    if (!tokenId || tokenId === 0) {
      // send MCash
      result = await this.sendTransaction(toAddress, amount, memo)
    } else if (typeof tokenId === 'number') {
      // send M1
      result = await window.mcashWeb.mcash.sendToken(toAddress, amount, tokenId, null, memo)
    } else if (typeof tokenId === 'string') {
      // return this.sendM20Token(toAddress, amount, tokenId, extra)
    }
    if (result) {
      const txId = result && result.transaction ? result.transaction.tx_id : null
      if (txId) {
        return this.getTransactionReceipt(txId)
      }
    }
    return Promise.reject(`Error sendToken ${tokenId}`)
  }

  async sendTransaction (to, amount, memo = '') {
    try {
      this.checkProvider()
      const result = await window.mcashWeb.mcash.sendTransaction(to, amount, undefined, memo)
      return Promise.resolve(result)
    } catch (e) {
      const errorMsg = e ? (typeof e === 'string' ? e : (e.message ? e.message : null)) : null
      return Promise.reject(errorMsg || 'Send transaction failed')
    }
  }

  getTransactionReceipt (txId) {
    return new Promise((resolve, reject) => {
      const retry = () => {
        setTimeout(() => this.getTransactionReceipt(txId), 300)
      }
      window.mcashWeb.mcash.getTransaction(txId)
        .then(receipt => {
          if (!receipt) {
            retry()
            return
          }
          const contractResult = receipt && Array.isArray(receipt.ret) && receipt.ret[0] ? receipt.ret[0].contract_result : ''
          if (['', 'OK', 'DEFAULT'].indexOf(contractResult.toUpperCase()) >= 0) {
            resolve(receipt)
          } else {
            reject(receipt)
          }
        })
        .catch(() => {
          retry()
        })
    })
  }

  async broadcastTransaction (transaction) {
    this.checkProvider()
    const signedTransaction = await window.mcashWeb.mcash.sign(transaction).catch(e => {
      console.error('error signing transaction: ', e)
      return false
    })
    if (signedTransaction) {
      const broadcast = await window.mcashWeb.mcash.sendRawTransaction(signedTransaction)
      if (!broadcast.result) {
        broadcast.result = false
        console.error('error when broadcast transaction ', broadcast)
        return Promise.reject('Error when broadcast transaction')
      }
      return Promise.resolve(broadcast)
    } else {
      return Promise.reject('Signing transaction failed')
    }
  }

  async triggerSmartContract (triggerSmartContract) {
    this.checkProvider()
    const { contract_address: contractAddress, function_selector: functionSelector, options, params } = triggerSmartContract || {}
    let unSignTransaction = await window.mcashWeb.transactionBuilder.triggerSmartContract(
      contractAddress,
      functionSelector,
      options,
      params
    )
    if (typeof unSignTransaction.transaction !== 'undefined') { unSignTransaction = unSignTransaction.transaction }
    return this.broadcastTransaction(unSignTransaction)
  }
}

export default new McashUtils()
