import { BigNumber } from 'bignumber.js'

export function isMidasWallet () {
  if (typeof navigator === 'undefined') return true
  return !!navigator.userAgent.match(/Midas/i)
}

export function toAmount (value, decimals = 8) {
  return value ? new BigNumber(value).multipliedBy(Math.pow(10, decimals)).toFixed() : ''
}

export function fromAmount (value, decimals = 8) {
  return value ? new BigNumber(value).div(Math.pow(10, decimals)).toFixed() : ''
}

export function serializeJSON (json = {}) {
  if (Object.keys(json).length === 0) {
    return ''
  } else {
    const parameters = Object.keys(json).filter(function (key) {
      return json[key] !== null
    })
    return parameters.map(function (keyName) {
      if (json[keyName] !== null) {
        return encodeURIComponent(keyName) + '=' + encodeURIComponent(json[keyName])
      }
    }).join('&')
  }
}

export function numberFormat (number, decimals = null, decPoint, thousandsSep, trailingZeros = false) {
  if (typeof number === 'undefined') return
  // Strip all characters but numerical ones.
  const numerical = (`${number}`).replace(/[^0-9+\-Ee.]/g, '')
  const n = !isFinite(+numerical) ? 0 : +numerical
  const prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
  const sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
  const dec = (typeof decPoint === 'undefined') ? '.' : decPoint
  let s = ''
  const toFixedFix = function (n, prec) {
    const k = Math.pow(10, prec)
    return `${Math.round(n * k) / k}`
  }
  // Fix for IE parseFloat(0.55).toFixed(0) = 0
  s = (decimals === null ? `${n}` : (prec ? toFixedFix(n, prec) : `${Math.round(n)}`)).split('.')
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || ''
    if (trailingZeros) {
      // 1.123 with decimals = 5 => 1.12300
      s[1] += new Array(prec - s[1].length + 1).join('0')
    }
  }
  return s[1] ? s.join(dec) : s[0]
}
