class CommunicationBus {
  constructor () {
    this.functions = {}
    this.intervals = {}
  }

  /**
   * Add a function to the list of available functions.
   * @param fn {Function}
   * @param index {String} - The name of the function normally
   */
  addFn (fn, index) {
    this.functions[index] = fn
    return index
  }

  /**
   * Set the Angular $interval for a function at a given index
   * @param index - Function identifier
   * @param millis - Timeout interval in milliseconds
   */
  startInterval = function (index, millis) {
    if (!this.intervals[index]) {
      this.intervals[index] = setInterval(this.functions[index], millis)
    }
  }

  /**
   * Stop an existing $interval
   * @param index - Function identifier
   */
  stopInterval (index) {
    if (this.intervals[index]) {
      clearInterval(this.intervals[index])
      delete this.intervals[index]
    }
  }

  /**
   * Retrieve a function
   * @param index - Function identifier
   */
  getFn (index) {
    return this.functions[index]
  }
}

export default new CommunicationBus()
