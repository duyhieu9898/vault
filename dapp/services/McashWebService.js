import McashWebModule from 'mcashweb'
import Common from './Common'
import { txDefault } from '../config/index'

class McashWebService {
  constructor () {
    this.coinbaseValue = null
    this.accounts = []
    this.mcashWeb = null
    this.keystore = null
    this.addresses = []
    this.store = null
    this._startupSetup()
  }

  get coinbase () {
    return this.coinbaseValue
  }

  set coinbase (value) {
    this.coinbaseValue = value
    if (this.store) {
      this.store.dispatch('mcashWebStore/setCoinBase', this.coinbaseValue)
    }
  }

  initStore (store) {
    this.store = store
  }

  _startupSetup () {
    // this.engine = new ProviderEngine()
    // this.mcashweb = new MultisigWeb3(this.engine)
    // if (this.getKeystore()) {
    //   this.engine.addProvider(new RpcSubprovider({
    //     rpcUrl: txDefault.mcashchainNode
    //   }))
    // }
    // this.engine.start()
  }

  webInitialized () {
    return this.reloadMcashWebProvider()
      .catch(e => {
        this.enableMcashWallet()
        return Promise.reject(e)
      })
  }

  getAccounts () {
    const address = this.mcashWeb ? this.mcashWeb.defaultAddress.base58 : null
    return Promise.resolve(address ? [address] : [])
  }

  /**
   * Asks McashLight to open its widget.
   * Returns a callback call with the list of accounts or null in case the
   * user rejects the approval request.
   * Return Promise(accounts, error)
   */
  enableMcashWallet () {
    return new Promise(async (resolve, reject) => {
      const toResolve = async () => {
        this.reloadMcashWebProvider()
          .then(resolve)
          .catch(reject)
        let accounts = await this.getAccounts()
        // // Convert to checksummed addresses
        accounts = this.toChecksumAddress(accounts)
        this.accounts = accounts
        this.coinbase = accounts[0]
        resolve(accounts)
      }
      const accounts = await this.getAccounts()
      if (accounts.length > 0) {
        toResolve()
      } else {
        let count = 0
        const retry = () => {
          const timeout = setTimeout(() => {
            count++
            if (count === 10) {
              clearTimeout(timeout)
              reject('Non-Mcashchain browser detected. You should consider trying McashLight!')
              return
            }
            if (this.getAccounts().length > 0) {
              toResolve()
            } else {
              retry()
            }
          }, 500)
        }
        retry()
      }
    })
  }

  /**
   * Returns true if McashLight is injected, false otherwise
   **/
  isMcashLightInjected () {
    return window && typeof window.mcashWeb !== 'undefined'
  }

  /**
   * Reloads mcashWeb provider
   * Return Promise()
   **/
  reloadMcashWebProvider () {
    this.accounts = []
    this.coinbase = null
    let mcashWeb = null
    if (window.mcashWeb) {
      mcashWeb = window.mcashWeb
    }
    if (!mcashWeb) {
      return Promise.reject('McashWeb Provider not connected')
    }

    return new Promise((resolve, reject) => {
      // Ledger wallet
      if (txDefault.wallet === 'ledger') {
        // if (Common.isElectron) {
        //   this.ledgerElectronSetup()
        //     .then(() => resolve())
        //     .catch(e => reject(e))
        // } else {
        //   this.ledgerSetup()
        //     .then(() => resolve())
        //     .catch(e => reject(e))
        // }
      } else if (txDefault.wallet === 'trezor') {
        // this.trezorSetup()
        // resolve()
      } else if (txDefault.wallet === 'injected' && mcashWeb && !Common.isElectron) {
        // injected mcashWeb provider (McashLight, etc)
        this.mcashWeb = mcashWeb
        // Set accounts
        // Convert to checksummed addresses
        this.getAccounts()
          .then(accounts => {
            this.accounts = this.toChecksumAddress(accounts)
            this.coinbase = accounts[0]
            resolve()
          })
          .catch(reject)
      } else if (txDefault.wallet === 'lightwallet' && Common.isElectron) {
        // this.lightWalletSetup()
        // resolve()
      } else if (txDefault.wallet === 'remotenode') {
        // Connect to Ethereum Node
        this.mcashWeb = new McashWebModule(txDefault.mcashchainNode)
        // Check connection
        try {
          this.mcashWeb.mcash.getNodeInfo()
            .then(async () => {
              // Set accounts
              let accounts = await this.getAccounts()
              // // Convert to checksummed addresses
              accounts = this.toChecksumAddress(accounts)
              this.accounts = accounts
              this.coinbase = accounts[0]

              if (resolve) {
                resolve()
              }
            })
            .catch(e => {
              // TODO: Utils.dangerAlert
              // Utils.dangerAlert('You are not connected to any node.')
              reject(e)
            })
        } catch (_) {
        }
      } else if (resolve) {
        resolve()
      }
    })
  }

  isAddress = (address) => {
    return this.mcashWeb && this.mcashWeb.isAddress(address)
  }

  isHexAddress = (address) => {
    return /^32|0x/.test(address) && address.length === 42
  }

  toHexAddress = (item) => {
    if (Array.isArray(item)) {
      return item.map(v => this.mcashWeb.address.toHex(v).replace(/^(32)/, '0x'))
    } else if (typeof item === 'string') {
      return this.mcashWeb.address.toHex(item).replace(/^(32)/, '0x')
    }
  }

  decodeAddress = (address) => {
    if (!address) {
      return address
    }
    // if (/^M/.test(address) && address.length === 34) {
    //   return address
    // }
    if (!this.mcashWeb) return address

    if (/^32|0x/.test(address) && address.length === 42) {
      return this.mcashWeb.address.fromHex(address)
    }
    return address
  }

  /**
   * Converts an object to a checksummed address when possible.
   * Accepts objects, strings and arrays.
   */
  toChecksumAddress = (item) => {
    let checkSummedItem
    if (item instanceof Array) {
      checkSummedItem = []
      for (let x = 0; x < item.length; x++) {
        checkSummedItem.push(this.decodeAddress(item[x]))
      }
    } else if (typeof item === 'string') {
      checkSummedItem = this.decodeAddress(item)
    } else if (typeof item === 'object') {
      checkSummedItem = {}
      let checkSummedKey
      Object.keys(item).forEach(key => {
        checkSummedKey = this.decodeAddress(key)
        checkSummedItem[checkSummedKey] = this.decodeAddress(item[key])

        // specific to Transaction object
        if (checkSummedItem[checkSummedKey] && checkSummedItem[checkSummedKey].info) {
          // Convert info object to checksummed
          checkSummedItem[checkSummedKey].info = this.toChecksumAddress(checkSummedItem[checkSummedKey].info)
        }
        if (checkSummedItem[checkSummedKey] && checkSummedKey === 'info') {
          // Convert info object to checksummed
          checkSummedItem[checkSummedKey] = this.toChecksumAddress(checkSummedItem[checkSummedKey])
        }
      })
    } else {
      return item
    }

    return checkSummedItem
  }

  sendTransaction (method, params, options, cb) {
    // Ugly but needed
    params[params.length - 1] = Object.assign({}, params[params.length - 1])

    // Simulate first
    // function sendIfSuccess (e, result) {
    //   if (e) {
    //     cb(e)
    //   } else {
    //     if (result) {
    //       method.sendTransaction.apply(method.sendTransaction, params.concat(cb))
    //     } else {
    //       const err = 'Simulated transaction failed'
    //       cb(err)
    //     }
    //   }
    // }

    // return new Promise((resolve, reject) => {
    //   if (options && options.onlySimulate) {
    //     //
    //   } else {}
    // })
    return method().call()

    // let args
    // if (options && options.onlySimulate) {
    //   args = params.concat(cb)
    //   method.call.apply(method.call, args)
    // } else {
    //   args = params.concat(sendIfSuccess)
    //   method.call.apply(method.call, args)
    // }
  }

  /**
   * Get mcashchain accounts and update account list.
   */
  updateAccounts () {
    return new Promise((resolve, reject) => {
      this.getAccounts()
        .then(res => {
          const accounts = this.toChecksumAddress(res)
          this.accounts = accounts.length ? accounts : []
          if (this.coinbase && this.accounts.indexOf(this.coinbase) !== -1) {
            // same coinbase
          } else if (this.accounts) {
            this.coinbase = this.accounts[0]
          } else {
            this.coinbase = null
          }
          resolve(accounts)
        })
        .catch(reject)
    })
  }

  // Open Info Modal
  openLedgerModal (isElectron) {
  }

  // Ledger setup on browser
  ledgerSetup () {
  }

  // Ledger wallet electron setup
  ledgerElectronSetup () {
  }

  // Trezor setup
  trezorSetup () {
  }

  /**
   * Select account
   * @param account {String} - Checksumed address
   **/
  selectAccount (account) {
    this.coinbase = account
  }

  /**
   * Sets Mcashweb up for the lightwallet
   */
  _lightwalletMcashWebSetup () {
  }

  /**
   * Light wallet setup
   * @param restore, default false
   * @param password, default null
   */
  lightWalletSetup (restore = false, password = null) {
  }

  /**
   * Creates a new keystore and within one account
   */
  createLightWallet (password, ctrlCallback) {
  }

  importLightWalletAccount (v3, password, ctrlCallback) {
  }

  /**
   * Decrypts a V3 keystore
   */
  decryptLightWallet (address, password, callback) {
  }

  /**
   * Restore keystore from localStorage
   * @param password,
   */
  restoreLightWallet (password) {
  }

  /**
   * Returns keystore string from localStorage or null
   */
  getKeystore () {
    return localStorage.getItem('keystore')
  }

  /**
   * Set keystore localStorage string
   */
  setKeystore (value) {
    // check wheter valus is a JSON valid format
    const valueToStore = JSON.stringify(value)
    localStorage.setItem('keystore', valueToStore)
  }

  /**
   * Checks whether input seed is valid or not
   */
  isSeedValid (seed) {
    // return lightwallet.keystore.isSeedValid(seed);
  }

  /**
   * Generates a new random seed
   */
  generateLightWalletSeed () {
    // return lightwallet.keystore.generateRandomSeed();
  }

  /**
   * Return accounts list
   */
  getLightWalletAddresses () {
  }

  async broadcastTransaction (transaction) {
    const signedTransaction = await this.mcashWeb.mcash.sign(transaction).catch(e => {
      console.error('error signing transaction: ', e)
      return false
    })
    if (signedTransaction) {
      const broadcast = await this.mcashWeb.mcash.sendRawTransaction(signedTransaction)
      if (!broadcast.result) {
        broadcast.result = false
        console.error('error when broadcast transaction ', broadcast)
        return Promise.reject('Error when broadcast transaction')
      }
      return Promise.resolve(broadcast)
    } else {
      return Promise.reject('Signing transaction failed')
    }
  }

  async triggerSmartContract (triggerSmartContract) {
    const { contractAddress, functionSelector, options, params } = triggerSmartContract || {}
    let unSignTransaction = await this.mcashWeb.transactionBuilder.triggerSmartContract(
      contractAddress,
      functionSelector,
      options,
      params
    )
    if (typeof unSignTransaction.transaction !== 'undefined') {
      unSignTransaction = unSignTransaction.transaction
    }
    return this.broadcastTransaction(unSignTransaction)
  }
}

export default new McashWebService()
