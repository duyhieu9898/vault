import McashWebService from './McashWebService'

class Contract {
  constructor () {
    this.types = {
      multisig: 'Multisig',
      token: 'Token',
      eoa: 'EOA',
      contract: 'Contract'
    }
  }

  getContract (address) {
    return McashWebService.mcashWeb.mcash.getContract(address)
  }

  async isContractAddress (address, contractData = null) {
    try {
      const data = contractData || await this.getContract(address) || {}
      return !!(data && Object.keys(data) > 0)
    } catch (e) {
      return false
    }
  }
}

export default new Contract()
