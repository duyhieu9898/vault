class Common {
  constructor () {
    // Detect if it's an Electron app or not
    this.isElectron = !!(window && window.process && window.process.type)
  }

  isValidHex (str) {
    return /[0-9A-Fa-f]{6}/g.test(str)
  }
}

export default new Common()
