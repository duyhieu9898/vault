import McashWebService from './McashWebService'
import Wallet from './Wallet'
import { txDefault } from '../config'

class Transaction {
  constructor () {
    this.requiredReceipt = {}
    this.requiredInfo = {}
    this.updates = 0
    this.callbacks = {}
    this.store = null
  }

  initStore (store) {
    this.store = store
  }

  triggerUpdates = () => {
    this.updates++
    this.store.dispatch('transactionStore/onIncreaseUpdates')
  }

  processReceipt = (receiptData) => {
    if (receiptData) {
      const receipt = McashWebService.toChecksumAddress(receiptData)
      receipt.decodedLogs = Wallet.decodeLogs(receipt.log)
      this.update(receipt.id, { receipt: receipt })

      // call callback if it has
      if (this.callbacks[receipt.id]) {
        // Execute callback function
        this.callbacks[receipt.id](receipt)
      }
    }
  }

  getTransactionInfo = (infoData) => {
    if (infoData) {
      // Convert info object to an object containing checksum addresses
      const info = McashWebService.toChecksumAddress(infoData)
      this.update(info.tx_id, { info: info })
    }
  }

  getTransactions () {
    return JSON.parse(localStorage.getItem('transactions')) || {}
  }

  /**
   * Add transaction object to the transactions collection
   */
  add = (txData) => {
    // Convert incoming object's addresses to checksummed ones
    const tx = McashWebService.toChecksumAddress(txData)

    const transactions = this.getTransactions()
    transactions[tx.txId] = tx
    if (tx.callback) {
      this.callbacks[tx.txId] = tx.callback
    }
    tx.date = new Date()
    localStorage.setItem('transactions', JSON.stringify(transactions))
    this.triggerUpdates()

    McashWebService.mcashWeb.mcash.getTransaction(tx.txId)
      .then(receipt => {
        this.getTransactionInfo(receipt)
        setTimeout(() => {
          this.checkReceipts()
        }, 2000)
      })
      .catch(error => {
        console.log(error)
      })
  }

  update = (txId, obj) => {
    const transactions = this.getTransactions()
    // Convert incoming object's addresses to checksummed ones
    const newObj = McashWebService.toChecksumAddress(obj)
    const tx = McashWebService.toChecksumAddress(txId)
    Object.assign(transactions[tx], newObj)
    localStorage.setItem('transactions', JSON.stringify(transactions))
    this.triggerUpdates()
  }

  notifyObservers = () => {
    this.triggerUpdates()
  }

  /**
   * Remove transaction identified by transaction hash from the transactions collection
   */
  remove = (txId) => {
    const transactions = this.getTransactions()
    delete transactions[txId]
    localStorage.setItem('transactions', JSON.stringify(transactions))
    this.triggerUpdates()
  }

  /**
   * Remove all transactions
   */
  removeAll = () => {
    localStorage.removeItem('transactions')
    this.triggerUpdates()
  }

  /**
   * Send transaction, signed by wallet service provider
   */
  send = (tx, cb) => {
    McashWebService.sendTransaction(
      McashWebService.mcashWeb.mcash,
      [tx],
      { onlySimulate: false },
      function (e, txId) {
        if (e) {
          cb(e)
        } else {
          this.add(
            {
              txId: txId,
              callback: function (receipt) {
                cb(null, receipt)
              }
            }
          )
          cb(null, txId)
        }
      }
    )
  }

  simulate (tx, cb) {
    const options = Object.assign({ onlySimulate: true }, tx)
    McashWebService.sendTransaction(
      McashWebService.mcashWeb.mcash,
      [tx],
      options,
      function (e, txId) {
        if (e) {
          cb(e)
        } else {
          cb(null, txId)
        }
      }
    )
  }

  /**
   * Sign transaction without sending it to an mcashchain node
   */
  signOffline (txObject, cb) {
    // Create transaction object
    const txInfo = {
      from: McashWebService.coinbase,
      to: txObject.to,
      value: txObject.value
    }

    McashWebService.mcashWeb.mcash.sign(txInfo, function (e, signed) {
      if (e) {
        cb(e)
      } else {
        cb(e, signed.raw)
      }
    })
  }

  /**
   * Sign transaction without sending it to an mcashchain node. Needs abi,
   * selected method to execute and related params.
   */
  signMethodOffline = (tx, abi, method, params, cb) => {
    // Get data
    const instance = McashWebService.mcashWeb.contract(abi).at(tx.to)
    tx.data = instance[method].getData.apply(this, params)
    this.signOffline(tx, cb)
  }

  /**
   * Send transaction, signed by wallet service provider. Needs abi,
   * selected method to execute and related params.
   */
  sendMethod = (tx, abi, method, params, cb) => {
    // Instance contract
    const instance = McashWebService.mcashWeb.contract(abi).at(tx.to)
    const transactionParams = params.slice()
    transactionParams.push(tx)

    try {
      McashWebService.sendTransaction(
        instance[method],
        transactionParams,
        { onlySimulate: false },
        function (e, txId) {
          if (e) {
            cb(e)
          } else {
            // Add transaction
            this.add({
              txId: txId,
              callback: function (receipt) {
                cb(null, receipt)
              }
            })
            cb(null, txId)
          }
        })
    } catch (e) {
      cb(e)
    }
  }

  simulateMethod (tx, abi, method, params, cb) {
    // Instance contract
    const instance = McashWebService.mcashWeb.contract(abi).at(tx.to)
    const options = Object.assign({ onlySimulate: true }, tx)

    try {
      McashWebService.sendTransaction(
        instance[method],
        params,
        options,
        function (e, txId) {
          if (e) {
            cb(e)
          } else {
            cb(null, txId)
          }
        })
    } catch (e) {
      cb(e)
    }
  }

  /**
   * Send signed transaction
   **/
  sendRawTransaction (tx) {
    return McashWebService.mcashWeb.mcash.sendRawTransaction(tx)
  }

  /**
   * Internal loop, checking for transaction receipts and transaction info.
   * calls callback after receipt is retrieved.
   */
  checkReceipts = () => {
    // Add transactions without receipt to batch request
    const transactions = this.getTransactions()
    const txIds = Object.keys(transactions)

    txIds.forEach(txId => {
      const tx = transactions[txId]
      // Get transaction receipt
      if (tx && !tx.receipt) {
        McashWebService.mcashWeb.mcash.getTransactionInfo(txId).then(this.processReceipt)
      }
      // Get transaction info
      if (tx && !tx.info) {
        McashWebService.mcashWeb.mcash.getTransaction(txId).then(this.getTransactionInfo)
      }
    })
  }

  getMcashchain () {
    if (!McashWebService.mcashWeb) {
      return Promise.resolve({})
    }
    let fullNode = McashWebService.mcashWeb.fullNode
    if (/.\/$/.test(fullNode)) {
      fullNode = fullNode.substr(0, fullNode.length - 1)
    }
    const data = {}
    switch (fullNode.host) {
      case 'https://mainnet.mcash.network':
        data.chain = 'mainnet'
        data.mcashscan = 'https://mcashscan.io/#'
        data.walletFactoryAddress = txDefault.walletFactoryAddresses.mainnet.address
        break
      case 'https://testnet.mcash.network':
        data.chain = 'testnet'
        data.mcashscan = 'https://testnet.mcashscan.io/#'
        data.walletFactoryAddress = txDefault.walletFactoryAddresses.testnet.address
        break
      case 'https://zeronet.mcash.network':
        data.chain = 'zeronet'
        data.mcashscan = 'https://zeronet.mcashscan.io/#'
        data.walletFactoryAddress = txDefault.walletFactoryAddresses.zeronet.address
        break
      default:
        data.chain = 'privatenet'
        data.mcashscan = 'https://privatenet.mcashscan.io/#'
        data.walletFactoryAddress = txDefault.walletFactoryAddresses.privatenet.address
    }
    return Promise.resolve(data)
  }

  async trigger (contractAddress, functionSelector, options, params) {
    let unSignTransaction = await McashWebService.mcashWeb.transactionBuilder.triggerSmartContract(
      contractAddress,
      functionSelector,
      options,
      params
    )
    if (typeof unSignTransaction.transaction !== 'undefined') { unSignTransaction = unSignTransaction.transaction }
    return unSignTransaction
  }
}

export default new Transaction()
