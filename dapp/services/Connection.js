import { txDefault } from '../config'

class Connection {
  constructor () {
    this.isConnected = false
    this.setup()
  }

  setup = function () {
    // Call it at startup
    this.checkConnection()
    // Setup interval
    setInterval(this.checkConnection, txDefault.connectionChecker.checkInterval)
  }

  // callDigest() {
  //   try{
  //     $rootScope.$digest();
  //   }catch(error){}
  // }

  /**
   * Connection lookup against a defined endpoint
   * Check config.js for the endpoint configuration
   */
  checkConnection () {
    this.isConnected = navigator.onLine // true | false
    // this.callDigest()
  }
}

export default new Connection()
