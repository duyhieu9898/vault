import abiDecoder from 'abi-decoder'

class ABI {
  constructor () {
    this.saved = JSON.parse(localStorage.getItem('abis')) || {}
  }

  get () {
    return JSON.parse(localStorage.getItem('abis')) || {}
  }

  update (abi, to, name) {
    abiDecoder.addABI(abi)
    this.saved[to] = { abi: abi, name: name }
    localStorage.setItem('abis', JSON.stringify(this.saved))
  }

  remove (to) {
    abiDecoder.removeABI(this.saved[to].abi)
    delete this.saved[to]
    localStorage.setItem('abis', JSON.stringify(this.saved))
  }

  decode (data) {
    const hexData = (data && !/^0x/.test(data) ? '0x' : '') + data
    const decoded = abiDecoder.decodeMethod(hexData)
    if (!decoded) {
      if (hexData.length > 20) {
        return {
          title: hexData.slice(0, 20) + '...',
          notDecoded: true
        }
      } else {
        return {
          title: hexData.slice(0, 20),
          notDecoded: true
        }
      }
    } else {
      return {
        title: decoded.name,
        params: decoded.params
      }
    }
  }
}

export default new ABI()
