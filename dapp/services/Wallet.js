import abiDecoder from 'abi-decoder'
import _ from 'lodash'
import ABI from './ABI'
import McashWebService from './McashWebService'
import Connection from './Connection'
import abiJSON from '../config/abi'
import { txDefault } from '../config'

class Wallet {
  constructor () {
    this.store = null
    this.wallets = JSON.parse(localStorage.getItem('wallets')) || {}
    this.json = abiJSON
    this.txParams = {}
    this.accounts = []
    this.methodIds = {}
    this.updates = 0
    this.mergedABI = []
    this.init()
  }

  initStore (store) {
    this.store = store
  }

  addMethods (abi) {
    abiDecoder.addABI(abi)
  }

  init () {
    this.mergedABI = this.json.multiSigDailyLimit.abi.concat(this.json.multiSigDailyLimitFactory.abi).concat(this.json.token.abi)
    // Concat cached abis
    const cachedABIs = ABI.get()
    Object.keys(cachedABIs).map((key) => {
      if (cachedABIs[key].abi) {
        this.mergedABI = this.mergedABI.concat(cachedABIs[key].abi)
      }
    })
    // Generate event id's
    this.addMethods(this.mergedABI)
  }

  /**
   * Returns all the wallets saved in the
   * Browser localStorage
   */
  getAllWallets () {
    try {
      return JSON.parse(localStorage.getItem('wallets')) || {}
    } catch (error) {
      return {}
    }
  }

  /**
   * Return tx object, with default values, overwritted by passed params
   **/
  txDefaults (tx) {
    const txParams = {
      from: McashWebService.coinbase
    }

    Object.assign(txParams, tx)
    return txParams
  }

  /**
   * Return mcash_call request object.
   * custom method .call() for direct calling.
   */
  callRequest (method, params, cb) {
    // Add to params the callback
    const methodParams = params.slice()
    methodParams.push(cb)

    // Get request object
    const request = method.request.apply(method, methodParams)
    request.call = function () {
      method.call.apply(method, methodParams)
    }
    return Object.assign({}, request, {
      method: 'mcash_call',
      params: [
        {
          to: request.params[0].to,
          data: request.params[0].data
        },
        'latest'
      ]
    })
  }

  /**
   * For a given address and data, sign a transaction offline
   */
  offlineTransaction (transaction) {
    return McashWebService.mcashWeb.mcash.sign(transaction)
  }

  // Init txParams
  initParams () {
    return new Promise((resolve, reject) => {
      McashWebService.updateAccounts()
        .finally(() => {
          return Promise.all([
            new Promise((resolve, reject) => {
              if (McashWebService.coinbase) {
                this.getBalance(McashWebService.coinbase)
                  .then(balance => {
                    this.balance = balance
                    resolve()
                  })
                  .catch(reject)
              } else {
                // If no coinbase is set, defaults to 0
                this.balance = 0
                resolve()
              }
            })
          ])
            .then(resolve)
            .catch(reject)
        })
    })
  }

  updateWallet (w) {
    const wallets = this.getAllWallets()
    const address = w.address
    if (!wallets[address]) {
      wallets[address] = {}
    }
    const tokens = {}
    if (w.tokens) {
      const tokenAddresses = Object.keys(w.tokens)
      tokenAddresses.map(function (item) {
        const token = w.tokens[item]
        tokens[token.address] = {
          name: token.name,
          symbol: token.symbol,
          decimals: token.decimals,
          address: token.address
        }
      })
    }

    // Converts the addresses to Checksumed addresses
    const owners = {}
    if (w.owners) {
      let checksumedAddress

      for (const x in w.owners) {
        checksumedAddress = McashWebService.toChecksumAddress(w.owners[x].address)
        owners[checksumedAddress] = w.owners[x]
      }
    }

    Object.assign(
      wallets[address], {
        address: address,
        name: w.name,
        owners: owners,
        tokens: tokens,
        safeMigrated: w.safeMigrated || false
      }
    )

    localStorage.setItem('wallets', JSON.stringify(wallets))
    this.triggerUpdates()
  }

  /**
   * Creates and returns the valid configuration for Import/Export purposes
   * @param jsonConfig
   * @param operation 'import' | 'export'
   */
  getValidConfigFromJSON (jsonConfig, operation) {
    /* JSON structure based on the following one
    *
    *  {
    *    "wallets" : {
    *      "wallet_address": {
    *        "name": "wallet_name",
    *        "address" : "wallet_address",
    *        "owners": {
    *          "address": "owner_address",
    *          "name" : "owner_name"
    *        },
    *        "tokens":{
    *           "token_address":{
    *              "address":"token_address",
    *              "name":"token_name",
    *              "symbol":"token_symbol",
    *              "decimals":token_decimals
    *           }
    *        }
    *      }
    *    },
    *    "abis" : {
    *        "address" : [ abi array ]
    *    },
    *    "addressBook": {
    *
    *    }
    *  }
    *
    */

    if (jsonConfig === {} || jsonConfig === '') {
      return {}
    }

    // Create th valid JSON input structure
    const validJsonConfig = {}
    validJsonConfig.wallets = {}
    validJsonConfig.abis = {}
    validJsonConfig.addressBook = {}

    if (!_.isEqual(jsonConfig.abis, {})) {
      validJsonConfig.abis = jsonConfig.abis
    } else {
      delete validJsonConfig.abis
    }

    if (!_.isEqual(jsonConfig.addressBook, {})) {
      validJsonConfig.addressBook = jsonConfig.addressBook
    } else {
      delete validJsonConfig.addressBook
    }

    if (!_.isEqual(jsonConfig.wallets, {})) {
      const walletKeys = Object.keys(jsonConfig.wallets)
      let ownerKeys = null
      let tokenKeys = null

      for (let x = 0; x < walletKeys.length; x++) {
        const owners = jsonConfig.wallets[walletKeys[x]].owners
        const tokens = jsonConfig.wallets[walletKeys[x]].tokens || []
        const validOwners = {}
        const validTokens = {}

        // Get tokens and owner keys
        tokenKeys = Object.keys(tokens)
        ownerKeys = Object.keys(owners)

        // Construct the valid JSON structure
        validJsonConfig.wallets[walletKeys[x]] = {
          name: jsonConfig.wallets[walletKeys[x]].name,
          owners: {},
          tokens: {}
        }

        // Add address key => value pair only when importing
        // configuration to adapt it to the App JSON Structure
        if (operation === 'import') {
          validJsonConfig.wallets[walletKeys[x]].address = walletKeys[x]
        }

        // Populate owners object
        for (let y = 0; y < ownerKeys.length; y++) {
          if (operation === 'import') {
            validOwners[ownerKeys[y]] = {
              name: owners[ownerKeys[y]] ? owners[ownerKeys[y]] : `Owner ${y + 1}`,
              address: ownerKeys[y]
            }
          } else {
            validOwners[ownerKeys[y]] = owners[ownerKeys[y]].name ? owners[ownerKeys[y]].name : ''
          }
        }

        Object.assign(validJsonConfig.wallets[walletKeys[x]].owners, validOwners)
        // Populate tokens object
        for (let k = 0; k < tokenKeys.length; k++) {
          validTokens[tokenKeys[k]] = {
            name: tokens[tokenKeys[k]].name,
            symbol: tokens[tokenKeys[k]].symbol,
            decimals: tokens[tokenKeys[k]].decimals
          }

          if (operation === 'import') {
            validTokens[tokenKeys[k]].address = tokenKeys[k]
          }

          Object.assign(validJsonConfig.wallets[walletKeys[x]].tokens, validTokens)
        }
      }
    } else {
      delete validJsonConfig.wallets
    }

    return validJsonConfig
  }

  /**
   * Imports a JSON configuration script containing
   * the wallet or wallets declarations
   */
  import (jsonConfig) {
    // Setting up new configuration
    // No data validation at the moment
    const walletsData = JSON.parse(localStorage.getItem('wallets')) || {}
    const validJsonConfig = this.getValidConfigFromJSON(JSON.parse(jsonConfig), 'import')
    // Object.assign doesn't create a new key => value pair if
    // the key already exists, so at the moment we execute the
    // entire JSON object returning OK to the user.
    Object.assign(walletsData, validJsonConfig.wallets)

    // Update abis if the key exists in the configuration object
    if (validJsonConfig.abis !== undefined) {
      const abiAddresses = Object.keys(validJsonConfig.abis)
      for (let x = 0; x < abiAddresses.length; x++) {
        ABI.update(validJsonConfig.abis[abiAddresses[x]].abi, abiAddresses[x], validJsonConfig.abis[abiAddresses[x]].name)
      }
    }

    this.wallets = this.toChecksummedWalletConfiguration(walletsData)
    // Save changes to `wallets`
    localStorage.setItem('wallets', JSON.stringify(this.wallets))
    // Save changes to `addressBook`
    if (validJsonConfig.addressBook) {
      // Convert addresses to checksum
      validJsonConfig.addressBook = McashWebService.toChecksumAddress(validJsonConfig.addressBook)
      localStorage.setItem('addressBook', JSON.stringify(validJsonConfig.addressBook))
    }
    this.triggerUpdates()
  }

  /**
   * Convert addresses to checksum addresses and returns the converted object
   */
  toChecksummedWalletConfiguration (walletsParams) {
    /*
    * {
    *   wallets: {
    *     "0x2d4ff1A416375B61Eb61124f673C4c44bA063140": {
    *       owners: {
    *         "0x1d4cc1A416375B61Eb61125f673D4c44bA063130": {
    *           address: "0x1d4cc1A416375B61Eb61125f673D4c44bA063130",
    *           name: "John"
    *         }
    *       },
    *       tokens: {
    *         "0x6810e776880C02933D47DB1b9fc05908e5386b96": {
    *           address: "0x6810e776880C02933D47DB1b9fc05908e5386b96",
    *           name: "GNO",
    *           symbol: "GNO",
    *           decimals: 18
    *         }
    *       }
    *     }
    *   }
    * }
    *
    */
    // Convert wallets' keys to checksum
    const walletsData = McashWebService.toChecksumAddress(walletsParams)
    Object.entries(walletsData).forEach(([walletKey, wallet]) => {
      walletsData[walletKey].address = McashWebService.toChecksumAddress(wallet.address || walletKey)
      const owners = {}
      const tokens = {}
      // Convert owners
      Object.entries(walletsData[walletKey].owners).forEach(([ownerKey, owner]) => {
        owners[McashWebService.toChecksumAddress(ownerKey)] = {
          address: McashWebService.toChecksumAddress(owner.address || owner),
          name: owner.name
        }
      })
      // Convert tokens
      Object.entries(walletsData[walletKey].tokens).forEach(([tokenKey, token]) => {
        tokens[McashWebService.toChecksumAddress(tokenKey)] = {}
        Object.assign(
          tokens[McashWebService.toChecksumAddress(token.address || token)],
          token,
          {
            address: McashWebService.toChecksumAddress(token.address || token)
          }
        )
      })
      walletsData[walletKey].owners = owners
      walletsData[walletKey].tokens = tokens
    })

    return walletsData
  }

  removeWallet (address) {
    const wallets = this.getAllWallets()
    delete wallets[address]
    this.wallets = wallets
    localStorage.setItem('wallets', JSON.stringify(wallets))
    this.triggerUpdates()
  }

  update (address, name) {
    const wallets = this.getAllWallets()
    wallets[address].name = name
    localStorage.setItem('wallets', JSON.stringify(wallets))
    this.triggerUpdates()
  }

  deployWithLimit (owners, requiredConfirmations, limit, cb) {
    const MyContract = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi)

    MyContract.new(
      owners,
      requiredConfirmations,
      limit,
      this.txDefaults({
        data: this.json.multiSigDailyLimit.binHex
      }),
      cb
    )
  }

  deployWithLimitFactory (owners, requiredConfirmations, limit) {
    // TODO: estimateGas
    const walletFactory = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimitFactory.abi, txDefault.walletFactoryAddress)
    try {
      const hexOwners = McashWebService.toHexAddress(owners)
      return walletFactory.methods.create(hexOwners, requiredConfirmations, limit).send()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  deployWithLimitFactoryOffline (owners, requiredConfirmations, limit, cb) {
    // const factory = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimitFactory.abi).at(txDefault.walletFactoryAddress)
    // const data = factory.create.getData(
    //   owners,
    //   requiredConfirmations,
    //   limit
    // )
    // this.offlineTransaction(txDefault.walletFactoryAddress, data, nonce, cb)
  }

  /**
   * Deploy wallet with daily limit
   **/
  deployWithLimitOffline (owners, requiredConfirmations, limit, cb) {
    // Get Transaction Data
    // const MyContract = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi)
    // const data = MyContract.new.getData(owners, requiredConfirmations, limit, {
    //   data: this.json.multiSigDailyLimit.binHex
    // })
    // this.offlineTransaction(null, data, nonce, cb)
  }

  getBalance (address) {
    if (McashWebService.mcashWeb) {
      return McashWebService.mcashWeb.mcash.getBalance(address)
    }
    return Promise.resolve(null)
  }

  async restore (info) {
    // console.log('kkk', this.json.multiSigDailyLimit.abi)
    const instance = await McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(info.address)
    // Check contract function works
    return new Promise((resolve, reject) => {
      instance.MAX_OWNER_COUNT().call()
        .then(count => {
          if ((!count && Connection.isConnected) || (count && count.eq(0) && Connection.isConnected)) {
            // it is not a wallet
            reject('Address ' + info.address + ' is not a wallet contract')
          } else {
            // Add wallet, add My account to the object by default, won't be
            // displayed anyway if user is not an owner, but if it is, name will be used
            if (McashWebService.coinbase) {
              const coinbase = McashWebService.toChecksumAddress(McashWebService.coinbase)
              info.owners = {}
              info.owners[coinbase] = { address: coinbase, name: 'My Account' }
            }
            this.updateWallet(info)
            resolve(info)
          }
        })
        .catch((e) => reject(e))
    })
  }

  // MultiSig functions

  /**
   * Get wallet owners
   */
  getOwners = (address) => {
    try {
      const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
      return instance.methods.getOwners().call()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /**
   * add owner to wallet
   */
  addOwner = (address, owner, options, cb) => {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    const data = instance.addOwner.getData(owner.address)

    return McashWebService.sendTransaction(
      instance.submitTransaction,
      [address, '0x0', data, this.txDefaults()],
      options,
      cb
    )
  }

  /**
   * Sign offline Add owner transaction
   */
  addOwnerOffline (address, owner, cb) {
    // const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    // const data = instance.addOwner.getData(owner.address)
    // const mainData = instance.submitTransaction.getData(address, '0x0', data)
    // this.offlineTransaction(address, mainData, nonce, cb)
  }

  /**
   * Get add owner transaction data
   **/
  getAddOwnerData (address, owner) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    return instance.addOwner.getData(owner.address)
  }

  /**
   * Remove owner
   */
  removeOwner (address, owner, options, cb) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    const data = instance.removeOwner.getData(owner.address)
    return McashWebService.sendTransaction(
      instance.submitTransaction,
      [address, '0x0', data, this.txDefaults()],
      options,
      cb
    )
  }

  /**
   * Get remove owner data
   **/
  getRemoveOwnerData (address, owner) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    return instance.removeOwner.getData(owner.address)
  }

  /**
   * Sign offline remove owner transaction
   **/
  removeOwnerOffline (address, owner, cb) {
    // const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    // const data = instance.removeOwner.getData(owner.address)
    // const mainData = instance.submitTransaction.getData(address, '0x0', data)
    // this.offlineTransaction(address, mainData, nonce, cb)
  }

  /**
   * Replace owner
   **/
  replaceOwner (address, owner, newOwner, options, cb) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    const data = instance.replaceOwner.getData(owner, newOwner)

    return McashWebService.sendTransaction(
      instance.submitTransaction,
      [address, '0x0', data, this.txDefaults()],
      options,
      cb
    )
  }

  /**
   * Sign replace owner offline
   **/
  replaceOwnerOffline (address, owner, newOwner, cb) {
    // const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    // const data = instance.replaceOwner.getData(owner, newOwner)
    // var mainData = instance.submitTransaction.getData(address, '0x0', data)
    // this.offlineTransaction(address, mainData, nonce, cb)
  }

  /**
   * Get required confirmations number
   */
  getRequired = (address) => {
    try {
      const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
      return instance.methods.required().call().then(v => Number(v))
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /**
   * Update confirmations
   */
  updateRequired = (address, required) => {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
    return instance.methods.changeRequirement(required).send()
    // TODO: submitTransaction
    // return McashWebService.sendTransaction(
    //   instance.submitTransaction,
    //   [address, '0x0', txId, this.txDefaults()],
    //   options,
    //   cb
    // )
  }

  getUpdateRequiredData (address, required) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
    return instance.changeRequirement.getData(required)
  }

  /**
   * Sign transaction offline
   */
  signUpdateRequired (address, required, cb) {
    // const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    // const data = instance.changeRequirement.getData(required)
    // const mainData = instance.submitTransaction.getData(address, '0x0', data)
    // this.offlineTransaction(address, mainData, nonce, cb)
  }

  /**
   * Get transaction hashes
   */
  getTransactionIds (address, from, to, pending, executed) {
    try {
      const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
      return instance.methods.getTransactionIds(from, to, pending, executed).call()
        .then(v => (v && v._transactionIds) || [])
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /**
   * Get transaction
   */
  getTransaction (address, txId) {
    try {
      const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
      return instance.methods.transactions(txId).call().then(tx => ({
        to: tx[0],
        value: '0x' + tx[1].toString(16),
        data: tx[2],
        id: txId,
        executed: tx[3]
      }))
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /**
   * Get confirmations
   */
  getConfirmations (address, txId) {
    try {
      const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
      return instance.methods.getConfirmations(txId).call().then(v => Number(v))
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /**
   * Get transaction count
   **/
  getTransactionCount = (address, pending, executed) => {
    try {
      const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
      return instance.methods.getTransactionCount(pending, executed).call().then(v => (v && Number(v.count)) || 0)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /**
   * Get daily limit
   **/
  getLimit = (address) => {
    try {
      const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
      return instance.methods.dailyLimit().call().then(v => Number(v))
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /**
   *
   **/
  calcMaxWithdraw = (address) => {
    try {
      const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
      return instance.methods.calcMaxWithdraw().call().then(v => Number(v))
    } catch (e) {
      return Promise.reject(e)
    }
  }

  /**
   * Change daily limit
   **/
  updateLimit (address, limit, options, cb) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
    return instance.methods.changeDailyLimit(limit).send()
    // TODO: submitTransaction
    // return McashWebService.sendTransaction(
    //   instance.submitTransaction,
    //   [address, '0x0', txId, this.txDefaults()],
    //   options,
    //   cb
    // )
  }

  /**
   * Get update limit transaction data
   **/
  getUpdateLimitData (address, limit) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
    return instance.changeDailyLimit.getData(limit)
  }

  /**
   * Sign update limit transaction
   **/
  signLimit (address, limit, cb) {
    // const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    // const data = instance.changeDailyLimit.getData(
    //   limit,
    //   cb
    // )
    // const mainData = instance.submitTransaction.getData(address, '0x0', data)
    // this.offlineTransaction(address, mainData, nonce, cb)
  }

  /**
   * Confirm transaction by another wallet owner
   */
  confirmTransaction (address, txId, options, cb) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi, address)
    return McashWebService.sendTransaction(
      instance.confirmTransaction,
      [txId, this.txDefaults()],
      options,
      cb
    )
  }

  /**
   * Sign confirm transaction offline by another wallet owner
   */
  confirmTransactionOffline (address, txId, cb) {
    // const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    // const mainData = instance.confirmTransaction.getData(txId)
    // this.offlineTransaction(address, mainData, nonce, cb)
  }

  /**
   * Execute multisig transaction, must be already signed by required owners
   */
  executeTransaction (address, txId, options, cb) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    McashWebService.sendTransaction(
      instance.executeTransaction,
      [txId, this.txDefaults()],
      options,
      cb
    )
  }

  /**
   * Signs transaction for execute multisig transaction, must be already signed by required owners
   */
  executeTransactionOffline (address, txId, cb) {
    // const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    // const mainData = instance.executeTransaction.getData(txId)
    // this.offlineTransaction(address, mainData, nonce, cb)
  }

  /**
   * Get confirmations
   */
  isConfirmed (address, txId, cb) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    return this.callRequest(
      instance.confirmations,
      [txId, McashWebService.coinbase],
      cb
    )
  }

  /**
   * Revoke transaction confirmation
   */
  revokeConfirmation (address, txId, options, cb) {
    const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    return McashWebService.sendTransaction(
      instance.revokeConfirmation,
      [txId, this.txDefaults()],
      options,
      cb
    )
  }

  /**
   * Revoke transaction confirmation offline
   */
  revokeConfirmationOffline (address, txId, cb) {
    // const instance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    // const data = instance.revokeConfirmation.getData(txId)
    // this.offlineTransaction(address, data, nonce, cb)
  }

  /**
   * Submit transaction
   **/
  submitTransaction (address, tx, abi, method, params, options, cb) {
    let data = '0x0'
    if (abi && method) {
      const instance = McashWebService.mcashWeb.contract(abi).at(tx.to)
      data = instance[method].getData.apply(this, params)
    }
    const walletInstance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    return McashWebService.sendTransaction(
      walletInstance.submitTransaction,
      [tx.to, tx.value, data, this.txDefaults()],
      options,
      cb
    )
  }

  /**
   * Sign offline multisig transaction
   **/
  signTransaction (address, tx, abi, method, params, cb) {
    // let data = '0x0'
    // if (abi && method) {
    //   const instance = McashWebService.mcashWeb.contract(abi).at(tx.to)
    //   data = instance[method].getData.apply(this, params)
    // }
    // const walletInstance = McashWebService.mcashWeb.contract(this.json.multiSigDailyLimit.abi).at(address)
    // const mainData = walletInstance.submitTransaction.getData(
    //   tx.to,
    //   tx.value,
    //   data
    // )
    // this.offlineTransaction(address, mainData, nonce, cb)
  }

  // Works as observer triggering for watch $scope
  triggerUpdates () {
    this.updates++
    this.store.dispatch('walletStore/onIncreaseUpdates')
  }

  /**
   * Returns a list of comprehensive logs, decoded from a list of encoded logs
   * Needs the abi to decode them
   **/
  decodeLogs (logs) {
    if (Array.isArray(logs) && logs.length) {
      const stringToHex = function (str) {
        return (/^0x/.test(str) ? '' : '0x') + str
      }
      // convert logs to base ethereum
      const convertedLogs = logs.map(item => ({
        ...item,
        address: stringToHex(item.address),
        topics: Array.isArray(item.topics) ? item.topics.map(v => stringToHex(v)) : item.topics,
        data: stringToHex(item.data)
      }))
      //
      return abiDecoder.decodeLogs(convertedLogs)
    }
    return null
  }
}

export default new Wallet()
