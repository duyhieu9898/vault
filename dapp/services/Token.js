import abiJSON from '../config/abi'
import McashWebService from './McashWebService'
import Wallet from './Wallet'
import { txDefault } from '../config'

class Token {
  constructor () {
    this.abi = abiJSON.token.abi
  }

  balanceOf (address, owner) {
    try {
      const instance = McashWebService.mcashWeb.contract(this.abi, address)
      return instance.methods.balanceOf(owner).call()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  name (address) {
    try {
      const instance = McashWebService.mcashWeb.contract(this.abi, address)
      return instance.methods.name().call()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  symbol (address) {
    try {
      const instance = McashWebService.mcashWeb.contract(this.abi, address)
      return instance.methods.symbol().call()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  decimals (address) {
    try {
      const instance = McashWebService.mcashWeb.contract(this.abi, address)
      return instance.methods.decimals().call()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  transfer (tokenAddress, toAddress, amount) {
    const instance = McashWebService.mcashWeb.contract(this.abi, tokenAddress)
    return instance.transfer(toAddress, amount).send()
  }

  transferOffline (tokenAddress, to, value, cb) {
    // const instance = McashWebService.mcashWeb.contract(this.abi).at(tokenAddress)
    // const data = instance.transfer.getData(to, value)
    // Wallet.offlineTransaction(tokenAddress, data, cb)
  }

  withdraw (tokenAddress, wallet, tx, options, cb) {
    const walletInstance = McashWebService.mcashWeb.contract(Wallet.json.multiSigDailyLimit.abi, wallet)
    const tokenInstance = McashWebService.mcashWeb.contract(this.abi, tokenAddress)
    const data = tokenInstance.transfer.getData(tx.to, tx.value)
    walletInstance.submitTransaction(
      tokenAddress,
      '0x0',
      data,
      Wallet.txDefaults(),
      options,
      cb)
  }

  withdrawOffline (tokenAddress, wallet, to, value, cb) {
    // const walletInstance = McashWebService.mcashWeb.contract(Wallet.json.multiSigDailyLimit.abi).at(wallet)
    // const tokenInstance = McashWebService.mcashWeb.contract(this.abi).at(tokenAddress)
    // const data = tokenInstance.transfer.getData(to, value)
    // const mainData = walletInstance.submitTransaction.getData(tokenAddress, '0x0', data)
    // Wallet.offlineTransaction(wallet, mainData, nonce, cb)
  }

  withdrawData (tokenAddress, to, value) {
    const tokenInstance = McashWebService.mcashWeb.contract(this.abi).at(tokenAddress)
    return tokenInstance.transfer.getData(to, value)
  }

  setDefaultTokens (address) {
    /**
     * Set all the default tokens to a given wallet address
     */
    const tokens = {}
    const wallets = Wallet.getAllWallets()

    txDefault.tokens.map(function (token) {
      if (!tokens[token.address]) {
        tokens[token.address] = {
          name: token.name,
          symbol: token.symbol,
          decimals: token.decimals,
          address: token.address
        }
      }
    })

    Object.assign(wallets[address].tokens, tokens)
    localStorage.setItem('wallets', JSON.stringify(wallets))
  }
}

export default new Token()
