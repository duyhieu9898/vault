import { txDefault } from '../config'

class Config {
  constructor () {
    this.updates = 0
  }

  getUserConfiguration () {
    return Object.assign({}, txDefault, JSON.parse(localStorage.getItem('userConfig')))
  }

  getConfiguration (key) {
    return JSON.parse(localStorage.getItem(key))
  }

  setConfiguration (key, value) {
    localStorage.setItem(key, value)
    this.triggerUpdates()
  }

  removeConfiguration (key) {
    localStorage.removeItem(key)
    this.triggerUpdates()
  };

  triggerUpdates () {
    this.updates++
  }
}

export default new Config()
