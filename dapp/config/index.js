import Common from '../services/Common'

const txDefaultOrig =
  {
    websites: {
      'wallet': 'https://wallet.gnosis.pm',
      'gnosis': 'https://gnosis.pm',
      'ethGasStation': 'https://safe-relay.gnosis.pm/api/v1/gas-station/'
    },
    resources: {
      'termsOfUse': 'https://wallet.gnosis.pm/TermsofUseMultisig.pdf',
      'privacyPolicy': 'https://gnosis.io/privacy-policy',
      'imprint': 'https://wallet.gnosis.pm/imprint.html'
    },
    gasLimit: 3141592,
    gasPrice: 18000000000,
    mcashNode: 'https://mainnet.mcash.network',
    connectionChecker: {
      method: 'OPTIONS',
      url: 'https://www.google.com',
      checkInterval: 5000
    },
    accountsChecker: {
      checkInterval: 5000
    },
    transactionChecker: {
      checkInterval: 15000
    },
    wallet: 'injected',
    defaultChainID: null,
    // Mainnet
    walletFactoryAddress: '0x6e95c8e8557abc08b46f3c347ba06f8dc012763f',
    tokens: [
      {
        address: 'MEKeo29kLUVEdckZ65F9ZnZGpgEDJpshsb',
        name: 'USDTM',
        symbol: 'USDTM',
        decimals: 6
      }
    ]
  }

if (Common.isElectron) {
  txDefaultOrig.wallet = 'remotenode'
}

const txDefault = {
  mcashNodes: [
    {
      url: 'https://mainnet.mcash.network',
      name: 'Remote Mainnet'
    },
    {
      url: 'https://testnet.mcash.network',
      name: 'Remote Testnet'
    },
    {
      url: 'https://zeronet.mcash.network',
      name: 'Remote Zeronet'
    }
  ],
  walletFactoryAddresses: {
    'mainnet': {
      name: 'Mainnet',
      address: txDefaultOrig.walletFactoryAddress
    },
    'testnet': {
      name: 'Testnet',
      address: ''
    },
    'zeronet': {
      name: 'Zeronet',
      address: 'MAyx5pN6WjZ1Em42e8UHMN6nTxyxQGmUBn'
    },
    'privatenet': {
      name: 'Privatenet',
      address: ''
    }
  }
}

const oldWalletFactoryAddresses = [
  ('0x12ff9a987c648c5608b2c2a76f58de74a3bf1987').toLowerCase(),
  ('0xed5a90efa30637606ddaf4f4b3d42bb49d79bd4e').toLowerCase(),
  ('0xa0dbdadcbcc540be9bf4e9a812035eb1289dad73').toLowerCase()
]

/**
 * Update the default wallet factory address in local storage
 */
function checkWalletFactoryAddress () {
  try {
    const userConfig = JSON.parse(localStorage.getItem('userConfig'))

    if (userConfig && oldWalletFactoryAddresses.indexOf(userConfig.walletFactoryAddress.toLowerCase()) >= 0) {
      userConfig.walletFactoryAddress = txDefaultOrig.walletFactoryAddress
      localStorage.setItem('userConfig', JSON.stringify(userConfig))
    }
  } catch (e) {
    console.error(e)
  }
}

/**
 * Reload configuration
 */
export function loadConfiguration () {
  const userConfig = JSON.parse(localStorage.getItem('userConfig'))
  Object.assign(txDefault, txDefaultOrig, userConfig)
}

checkWalletFactoryAddress()
loadConfiguration()

export {
  txDefaultOrig,
  txDefault
}
