export default [{
  'inputs': [],
  'constant': true,
  'name': 'defaultOperators',
  'outputs': [{ 'type': 'address[]' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'inputs': [],
  'constant': true,
  'name': 'name',
  'outputs': [{ 'type': 'string' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'deprecate',
  'inputs': [{ 'name': '_upgradedAddress', 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'approve',
  'inputs': [{ 'name': '_spender', 'type': 'address' }, { 'name': '_value', 'type': 'uint256' }],
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'inputs': [],
  'constant': true,
  'name': 'deprecated',
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'addBlackList',
  'inputs': [{ 'name': '_evilUser', 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'inputs': [],
  'constant': true,
  'name': 'totalSupply',
  'outputs': [{ 'type': 'uint256' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'removeWhiteList',
  'inputs': [{ 'name': '_notmintableAddress', 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'redeem',
  'inputs': [{ 'name': 'amount', 'type': 'uint256' }, { 'name': 'offchain', 'type': 'bytes32' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'transferFrom',
  'inputs': [{ 'name': '_from', 'type': 'address' }, { 'name': '_to', 'type': 'address' }, {
    'name': '_value',
    'type': 'uint256'
  }],
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'inputs': [],
  'constant': true,
  'name': 'upgradedAddress',
  'outputs': [{ 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'inputs': [],
  'constant': true,
  'name': 'decimals',
  'outputs': [{ 'type': 'uint8' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'inputs': [],
  'constant': true,
  'name': 'maximumFee',
  'outputs': [{ 'type': 'uint256' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'inputs': [],
  'name': 'unpause',
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'inputs': [],
  'constant': true,
  'name': 'isOwnerOrAdmin',
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'inputs': [],
  'constant': true,
  'name': 'granularity',
  'outputs': [{ 'type': 'uint256' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'constant': true,
  'name': 'getBlackListStatus',
  'inputs': [{ 'name': '_maker', 'type': 'address' }],
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'inputs': [],
  'constant': true,
  'name': 'paused',
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'operatorSend',
  'inputs': [{ 'name': 'sender', 'type': 'address' }, { 'name': 'recipient', 'type': 'address' }, {
    'name': 'amount',
    'type': 'uint256'
  }, { 'name': 'data', 'type': 'bytes' }, { 'name': 'operatorData', 'type': 'bytes' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'constant': true,
  'name': 'getWhiteListStatus',
  'inputs': [{ 'name': '_maker', 'type': 'address' }],
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'issue',
  'inputs': [{ 'name': 'amount', 'type': 'uint256' }, { 'name': 'offchain', 'type': 'bytes32' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'constant': true,
  'name': 'isWhiteListed',
  'inputs': [{ 'type': 'address' }],
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'setAdmin',
  'inputs': [{ 'name': 'newAdmin', 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'constant': true,
  'name': 'balanceOf',
  'inputs': [{ 'name': 'who', 'type': 'address' }],
  'outputs': [{ 'type': 'uint256' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'inputs': [],
  'name': 'renounceOwnership',
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'inputs': [],
  'name': 'pause',
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'inputs': [],
  'constant': true,
  'name': 'getOwner',
  'outputs': [{ 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'inputs': [],
  'constant': true,
  'name': 'owner',
  'outputs': [{ 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'inputs': [],
  'constant': true,
  'name': 'isOwner',
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'authorizeOperator',
  'inputs': [{ 'name': 'operator', 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'inputs': [],
  'constant': true,
  'name': 'symbol',
  'outputs': [{ 'type': 'string' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'redeemFrom',
  'inputs': [{ 'name': 'burner', 'type': 'address' }, { 'name': 'amount', 'type': 'uint256' }, {
    'name': 'offchain',
    'type': 'bytes32'
  }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'send',
  'inputs': [{ 'name': 'recipient', 'type': 'address' }, { 'name': 'amount', 'type': 'uint256' }, {
    'name': 'data',
    'type': 'bytes'
  }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'issueTo',
  'inputs': [{ 'name': 'recipient', 'type': 'address' }, { 'name': 'amount', 'type': 'uint256' }, {
    'name': 'offchain',
    'type': 'bytes32'
  }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'transfer',
  'inputs': [{ 'name': '_to', 'type': 'address' }, { 'name': '_value', 'type': 'uint256' }],
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'setParams',
  'inputs': [{ 'name': 'newBasisPoints', 'type': 'uint256' }, { 'name': 'newMaxFee', 'type': 'uint256' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'constant': true,
  'name': 'isOperatorFor',
  'inputs': [{ 'name': 'operator', 'type': 'address' }, { 'name': 'tokenHolder', 'type': 'address' }],
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'constant': true,
  'name': 'allowance',
  'inputs': [{ 'name': '_owner', 'type': 'address' }, { 'name': '_spender', 'type': 'address' }],
  'outputs': [{ 'name': 'remaining', 'type': 'uint256' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'inputs': [],
  'constant': true,
  'name': 'basisPointsRate',
  'outputs': [{ 'type': 'uint256' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'constant': true,
  'name': 'isBlackListed',
  'inputs': [{ 'type': 'address' }],
  'outputs': [{ 'type': 'bool' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'removeBlackList',
  'inputs': [{ 'name': '_clearedUser', 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'inputs': [],
  'constant': true,
  'name': 'MAX_UINT',
  'outputs': [{ 'type': 'uint256' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'addWhiteList',
  'inputs': [{ 'name': '_mintableAddress', 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'transferOwnership',
  'inputs': [{ 'name': 'newOwner', 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'destroyBlackFunds',
  'inputs': [{ 'name': '_blackListedUser', 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'inputs': [],
  'constant': true,
  'name': 'admin',
  'outputs': [{ 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'view'
},
{
  'name': 'revokeOperator',
  'inputs': [{ 'name': 'operator', 'type': 'address' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'operatorBurn',
  'inputs': [{ 'name': 'account', 'type': 'address' }, { 'name': 'amount', 'type': 'uint256' }, {
    'name': 'data',
    'type': 'bytes'
  }, { 'name': 'operatorData', 'type': 'bytes' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'name': 'burn',
  'inputs': [{ 'name': 'amount', 'type': 'uint256' }, { 'name': 'data', 'type': 'bytes' }],
  'type': 'function',
  'stateMutability': 'nonpayable'
},
{
  'inputs': [{ 'name': '_initialSupply', 'type': 'uint256' }, {
    'name': '_name',
    'type': 'string'
  }, { 'name': '_symbol', 'type': 'string' }, { 'name': '_decimals', 'type': 'uint8' }],
  'type': 'constructor',
  'stateMutability': 'nonpayable'
},
{
  'name': 'Issue',
  'inputs': [{ 'name': 'recipient', 'type': 'address' }, { 'name': 'amount', 'type': 'uint256' }, {
    'name': 'offchain',
    'type': 'bytes32'
  }],
  'type': 'event'
},
{
  'name': 'Redeem',
  'inputs': [{ 'name': 'burner', 'type': 'address' }, { 'name': 'amount', 'type': 'uint256' }, {
    'name': 'offchain',
    'type': 'bytes32'
  }],
  'type': 'event'
},
{
  'name': 'Deprecate',
  'inputs': [{ 'name': 'newAddress', 'type': 'address' }],
  'type': 'event'
},
{
  'name': 'Params',
  'inputs': [{ 'name': 'feeBasisPoints', 'type': 'uint256' }, { 'name': 'maxFee', 'type': 'uint256' }],
  'type': 'event'
},
{
  'name': 'AddedWhiteList',
  'inputs': [{ 'name': '_address', 'type': 'address' }],
  'type': 'event'
},
{
  'name': 'RemovedWhiteList',
  'inputs': [{ 'name': '_address', 'type': 'address' }],
  'type': 'event'
},
{
  'name': 'DestroyedBlackFunds',
  'inputs': [{ 'name': '_blackListedUser', 'type': 'address' }, { 'name': '_balance', 'type': 'uint256' }],
  'type': 'event'
},
{
  'name': 'AddedBlackList',
  'inputs': [{ 'name': '_user', 'type': 'address' }],
  'type': 'event'
},
{
  'name': 'RemovedBlackList',
  'inputs': [{ 'name': '_user', 'type': 'address' }],
  'type': 'event'
},
{
  'name': 'Transfer',
  'inputs': [{ 'indexed': true, 'name': 'from', 'type': 'address' }, {
    'indexed': true,
    'name': 'to',
    'type': 'address'
  }, { 'name': 'value', 'type': 'uint256' }],
  'type': 'event'
},
{
  'name': 'Approval',
  'inputs': [{ 'indexed': true, 'name': 'owner', 'type': 'address' }, {
    'indexed': true,
    'name': 'spender',
    'type': 'address'
  }, { 'name': 'value', 'type': 'uint256' }],
  'type': 'event'
},
{
  'name': 'Sent',
  'inputs': [{ 'indexed': true, 'name': 'operator', 'type': 'address' }, {
    'indexed': true,
    'name': 'from',
    'type': 'address'
  }, { 'indexed': true, 'name': 'to', 'type': 'address' }, { 'name': 'amount', 'type': 'uint256' }, {
    'name': 'data',
    'type': 'bytes'
  }, { 'name': 'operatorData', 'type': 'bytes' }],
  'type': 'event'
},
{
  'name': 'Minted',
  'inputs': [{ 'indexed': true, 'name': 'operator', 'type': 'address' }, {
    'indexed': true,
    'name': 'to',
    'type': 'address'
  }, { 'name': 'amount', 'type': 'uint256' }, { 'name': 'data', 'type': 'bytes' }, {
    'name': 'operatorData',
    'type': 'bytes'
  }],
  'type': 'event'
},
{
  'name': 'Burned',
  'inputs': [{ 'indexed': true, 'name': 'operator', 'type': 'address' }, {
    'indexed': true,
    'name': 'from',
    'type': 'address'
  }, { 'name': 'amount', 'type': 'uint256' }, { 'name': 'data', 'type': 'bytes' }, {
    'name': 'operatorData',
    'type': 'bytes'
  }],
  'type': 'event'
},
{
  'name': 'AuthorizedOperator',
  'inputs': [{ 'indexed': true, 'name': 'operator', 'type': 'address' }, {
    'indexed': true,
    'name': 'tokenHolder',
    'type': 'address'
  }],
  'type': 'event'
},
{
  'name': 'RevokedOperator',
  'inputs': [{ 'indexed': true, 'name': 'operator', 'type': 'address' }, {
    'indexed': true,
    'name': 'tokenHolder',
    'type': 'address'
  }],
  'type': 'event'
},
{
  'inputs': [],
  'name': 'Pause',
  'type': 'event'
},
{
  'inputs': [],
  'name': 'Unpause',
  'type': 'event'
},
{
  'name': 'OwnershipTransferred',
  'inputs': [{ 'indexed': true, 'name': 'previousOwner', 'type': 'address' }, {
    'indexed': true,
    'name': 'newOwner',
    'type': 'address'
  }],
  'type': 'event'
}]
