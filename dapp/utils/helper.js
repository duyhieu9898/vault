export function numberFormat (number, decimals = null, decPoint, thousandsSep, trailingZeros = false) {
  if (typeof number === 'undefined') return
  // Strip all characters but numerical ones.
  const numerical = (`${number}`).replace(/[^0-9+\-Ee.]/g, '')
  const n = !isFinite(+numerical) ? 0 : +numerical
  const prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
  const sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
  const dec = (typeof decPoint === 'undefined') ? '.' : decPoint
  let s = ''
  const toFixedFix = function (n, prec) {
    const k = Math.pow(10, prec)
    return `${Math.round(n * k) / k}`
  }
  // Fix for IE parseFloat(0.55).toFixed(0) = 0
  s = (decimals === null ? `${n}` : (prec ? toFixedFix(n, prec) : `${Math.round(n)}`)).split('.')
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || ''
    if (trailingZeros) {
      // 1.123 with decimals = 5 => 1.12300
      s[1] += new Array(prec - s[1].length + 1).join('0')
    }
  }
  return s[1] ? s.join(dec) : s[0]
}

export function isMobile () {
  if (typeof navigator === 'undefined' || typeof window === 'undefined') return false
  const isAndroid = navigator.userAgent.match(/Android/i)
  const isBlackBerry = navigator.userAgent.match(/BlackBerry/i)
  const isIOS = navigator.userAgent.match(/iPhone|iPad|iPod/i)
  const isOpera = navigator.userAgent.match(/Opera Mini/i)
  const isWindows = navigator.userAgent.match(/IEMobile/i)
  return isAndroid || isBlackBerry || isIOS || isOpera || isWindows
}

export function mobileDetection () {
  if (typeof navigator === 'undefined') return {}
  if (navigator.userAgent.match(/Android/i)) {
    return {
      name: 'Android',
      wallet: 'https://midaswallet.page.link/dapps'
    }
  } else if (navigator.userAgent.match(/BlackBerry/i)) {
    return {
      name: 'BlackBerry',
      wallet: ''
    }
  } else if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
    return {
      name: 'iOS',
      wallet: 'https://midaswallet.page.link/pUjXH5dRhNcFJjqEA'
    }
  } else if (navigator.userAgent.match(/Opera Mini/i)) {
    return {
      name: 'Opera',
      wallet: ''
    }
  } else if (navigator.userAgent.match(/IEMobile/i)) {
    return {
      name: 'Windows',
      wallet: ''
    }
  } else {
    return {
      name: 'unknown',
      wallet: ''
    }
  }
}

export function browserDetectionMcashLight () {
  if (typeof navigator === 'undefined') return {}
  if (isMobile()) {
    return mobileDetection()
  }
  return {
    name: 'Chrome',
    wallet: 'http://bit.ly/mcashchain-chrome-extension'
  }
}

export function copyToClipboard (str) {
  let el = str
  if (typeof str === 'string') {
    el = document.createElement('textarea')
    el.value = str
    el.setAttribute('readonly', '')
    el.style.position = 'absolute'
    el.style.top = '0'
    el.style.left = '-9999px'
    el.style.zIndex = '-1'
    document.body.appendChild(el)
  }

  if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
    // save current contentEditable/readOnly status
    const editable = el.contentEditable
    const readOnly = el.readOnly

    // convert to editable with readonly to stop iOS keyboard opening
    el.contentEditable = true
    el.readOnly = true

    // create a selectable range
    const range = document.createRange()
    range.selectNodeContents(el)

    // select the range
    const selection = window.getSelection()
    selection.removeAllRanges()
    selection.addRange(range)
    el.setSelectionRange(0, 999999)

    // restore contentEditable/readOnly to original state
    el.contentEditable = editable
    el.readOnly = readOnly
  } else {
    // el.focus()
    el.select()
  }

  document.execCommand('copy')
  if (typeof str === 'string') {
    document.body.removeChild(el)
  }
}

export function abbreviateNumber (number, digits = 2, min = 0) {
  if (!number || isNaN(number)) return number
  const num = +number
  if (num < min) {
    return numberFormat(num)
  }

  const units = ['K', 'M', 'B', 'T', 'P', 'E', 'Z', 'Y']
  let decimal = 0

  for (let i = units.length - 1; i >= 0; i--) {
    decimal = Math.pow(1000, i + 1)

    if (num <= -decimal || num >= decimal) {
      return +(num / decimal).toFixed(digits) + units[i]
    }
  }

  return num
}

export function shortAddress (address) {
  if (!address) {
    return ''
  }
  if (address.length <= 15) {
    return address
  }
  if (address.length === 34 && address.match(/^M/)) {
    return address.slice(0, 5) + '...' + address.slice(address.length - 3)
  }
  return address.slice(0, 12) + '...'
}

// --- get value in object ---
// extends object-path
const options = {}

function getKey (key) {
  const intKey = parseInt(key)
  if (intKey.toString() === key) {
    return intKey
  }
  return key
}

function hasOwnProperty (obj, prop) {
  if (obj === null) {
    return false
  }
  // to handle objects with null prototypes (too edge case?)
  return Object.prototype.hasOwnProperty.call(obj, prop)
}

function hasShallowProperty (obj, prop) {
  return (options.includeInheritedProps || (typeof prop === 'number' && Array.isArray(obj)) || hasOwnProperty(obj, prop))
}

function getShallowProperty (obj, prop) {
  if (hasShallowProperty(obj, prop)) {
    return obj[prop]
  }
}

export function gets (obj, curPath, defaultValue = null) {
  let path = curPath
  if (typeof path === 'number') {
    path = [path]
  }
  if (!path || path.length === 0) {
    return obj
  }
  if (obj === null) {
    return defaultValue
  }
  if (typeof path === 'string') {
    return gets(obj, path.split('.'), defaultValue)
  }

  const currentPath = getKey(path[0])
  const nextObj = getShallowProperty(obj, currentPath)
  if (typeof nextObj === 'undefined') {
    return defaultValue
  }

  if (path.length === 1) {
    return nextObj
  }

  return gets(obj[currentPath], path.slice(1), defaultValue)
}
// --- end get value in object ---
