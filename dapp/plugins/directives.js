import Vue from 'vue'
import Wallet from '../services/Wallet'
import McashWebService from '../services/McashWebService'

Vue.directive('disabled-if-no-accounts', function (el, binding, vnode) {
  vnode.context.$watch(function () {
    return vnode.context.$store.getters['mcashWebStore/coinbase']
  }, val => {
    if (val) {
      el.removeAttribute('disabled')
    } else {
      el.setAttribute('disabled', 'disabled')
    }
  })
})

// Disables an element when no accounts are set up
// or a wallet was not created on the current network
Vue.directive('disabled-if-no-accounts-or-wallet-available', function (el, binding, vnode) {
  vnode.context.$watch(function () {
    return vnode.context.$store.getters['walletStore/wallets'][binding.value].maxWithdraw
  }, () => {
    const currentWallet = vnode.context.$store.getters['walletStore/wallets'][binding.value]
    if (currentWallet && currentWallet.isOnChain) {
    } else if (binding.value) {
      Wallet.getOwners(binding.value)
        .then((owners) => {
          if (owners.length > 0 && vnode.context.$store.getters['mcashStore/addressMcash']) {
            el.removeAttribute('disabled')
          } else {
            el.setAttribute('disabled', 'disabled')
          }
        })
        .catch(() => {
          el.setAttribute('disabled', 'disabled')
        })
    } else {
      vnode.context.$watch(function () {
        if (vnode.context.$store.getters['mcashWebStore/coinbase']) {
          el.removeAttribute('disabled')
        } else {
          el.setAttribute('disabled', 'disabled')
        }
      })
    }
  }, { immediate: true })
})

Vue.directive('show-hide-by-connectivity', function (el, binding, vnode) {
  vnode.context.$watch(function () {
    return vnode.context.$store.getters['connectionStore/isConnected']
  }, val => {
    if (!val && binding.value === 'online') {
      el.style.display = 'none'
    } else if (val && binding.value === 'offline') {
      el.style.display = 'none'
    } else {
      el.style.display = null
    }
  }, { immediate: true })
})

Vue.directive('disabled-if-invalid-address', function (el, binding, vnode) {
  el.setAttribute('disabled', 'disabled')
  let isAddressValid = false
  if (!binding.value) {
    return
  }
  if (binding.value && binding.value.length < 34) {
    isAddressValid = false
  } else if (McashWebService.isAddress(binding.value) ||
      McashWebService.isAddress(McashWebService.toChecksumAddress(binding.value))) {
    isAddressValid = true
  } else {
    isAddressValid = false
  }

  // Set form as invalid
  if (isAddressValid) {
    el.removeAttribute('disabled')
  }
})
