import Vue from 'vue'
import CDialog from '../components/common/CDialog'
import CTooltip from '../components/common/CTooltip'

Vue.component('c-dialog', CDialog)
Vue.component('c-tooltip', CTooltip)
