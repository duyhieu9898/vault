import Vue from 'vue'
import { BigNumber } from 'bignumber.js'
import Wallet from '../services/Wallet'
import McashWebService from '../services/McashWebService'
import { fromAmount, numberFormat } from '../lib/multicoin-utils/common'

Vue.filter('stringLimitTo', function (text, limit) {
  return text ? (text.length > limit ? text.substr(0, limit) + '...' : text) : ''
})

Vue.filter('address', function (address) {
  if (address && address.length > 3) {
    return address.slice(0, 12) + '...'
  }
})

Vue.filter('mcash', function (num) {
  if (num || num === 0) {
    let formattedValue = '0.00'
    const mcash = new BigNumber(fromAmount(num))
    if (mcash.gt(0)) {
      const precision = mcash.gt(1) ? Math.floor(Math.log(mcash.toNumber()) / Math.log(10) + 3) : 2
      formattedValue = numberFormat(mcash.toString(), precision)
    }
    return formattedValue + ' MCASH'
  }
  return null
})

Vue.filter('logParam', function (log, isMcash) {
  if (log && Array.isArray(log)) {
    return log.reduce(function (finalString, address) {
      const decodeAddress = McashWebService.toChecksumAddress(address)
      if (!/^M/.test(decodeAddress)) {
        return finalString + decodeAddress + ', '
      } else {
        return finalString + decodeAddress.slice(0, 20) + '..., '
      }
    }, '').slice(0, -2)
  } else if (log === '0x0') {
    return '0x0'
  } else if (log && log.indexOf && log.indexOf('0x') !== -1) {
    const newLog = McashWebService.isHexAddress(log) ? McashWebService.toChecksumAddress(log) : log
    return newLog.slice(0, 10) + '...'
  } else if (log && log.match(/^[0-9]+$/) !== null) {
    if (isMcash) {
      return Vue.filter('mcash')(log)
    } else if (log.toString().length < 8) {
      return log.toString().slice(0, 7)
    } else {
      return new BigNumber(log).toExponential(3)
    }
  } else {
    return log
  }
})

Vue.filter('addressCanBeOwner', function (addressCandidate, currentWallet) {
  if (addressCandidate && Array.isArray(addressCandidate)) {
    for (const key in Wallet.wallets) {
      const wallet = Wallet.wallets[key]
      return addressCandidate.map(function (address) {
        if (wallet && wallet.owners && wallet.owners[address] && wallet.owners[address].name) {
          return wallet.owners[address].name
        } else {
          return address
        }
      })
    }
  } else if (addressCandidate && addressCandidate.indexOf && addressCandidate.indexOf('0x') !== -1 &&
    currentWallet && currentWallet.owners &&
    currentWallet.owners[addressCandidate] && currentWallet.owners[addressCandidate].name) {
    // If address is owner of the current wallet, return its name
    return currentWallet.owners[addressCandidate].name
  } else if (addressCandidate && addressCandidate.indexOf && addressCandidate.indexOf('0x') !== -1) {
    Object.entries(Wallet.wallets).forEach(function ([key, wallet]) {
      if (wallet && wallet.owners && wallet.owners[addressCandidate] && wallet.owners[addressCandidate].name) {
        return wallet.owners[addressCandidate].name
      }
    })
    return addressCandidate
  } else {
    return addressCandidate
  }
})

Vue.filter('mcashAddress', function (address) {
  return McashWebService.toChecksumAddress(address)
})

Vue.filter('token', function (token) {
  if (token && token.balance) {
    let decimals = token.decimals
    if (token.decimals === undefined) {
      decimals = 18
    }
    const stringSplit = new BigNumber(token.balance).div('1e' + decimals).toString(10).split('.')
    let newString = ''
    const places = stringSplit[0].length - 1
    for (let i = places; i >= 0; i--) {
      newString = stringSplit[0][i] + newString
      if (i > 0 && (places - i + 1) % 3 === 0) {
        newString = ',' + newString
      }
    }
    if (stringSplit.length === 2) {
      newString += '.' + stringSplit[1].substring(0, 2)
    }
    return newString + ' ' + token.symbol
  } else if (token && token.symbol) {
    return '0 ' + token.symbol
  } else {
    return null
  }
})
